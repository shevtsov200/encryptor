# Encryptor
> An app for encrypting and decrypting text using various encryption algorithms.

## Table of contents
* [Screenshots](#screenshots)
* [Technologies](#technologies)
	+ [Android](#android)
    + [Architecture](#architecture)
    + [Testing](#testing)

## Screenshots
![App screenshots](./images/screenshots.png)

## Technologies

### Android
* [Data Binding](https://developer.android.com/topic/libraries/architecture/index.html)
A support library that allows you to bind UI components in your layouts to data sources in your app using a declarative format rather than programmatically.
* [Material Design Components](https://material.io/develop/android/)
A set of UI Components used to build materil design apps.

### Architecture
* [Dagger2](https://github.com/google/dagger)
A compile-time framework for dependency injection.
* [Android Architecture Components](https://developer.android.com/topic/libraries/architecture)
Android architecture components are a collection of libraries that help you design robust, testable, and maintainable apps.

### Testing
* [JUnit 4](https://junit.org/junit4/)
A simple framework to write repeatable tests. It is an instance of the xUnit architecture for unit testing frameworks.
* [Mockito](https://site.mockito.org/)
A mocking framework for unit tests written in Java
* [Espresso](https://developer.android.com/training/testing/espresso)
A UI testing framework for Android

