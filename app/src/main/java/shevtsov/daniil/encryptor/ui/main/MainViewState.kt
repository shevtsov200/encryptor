package shevtsov.daniil.encryptor.ui.main

data class MainViewState(
        val sourceText: String,
        val characterCount: Int,
        val maxCharacters: Int,
        val isValidForEncryption: Boolean,
        val isValidForDecryption: Boolean
)
