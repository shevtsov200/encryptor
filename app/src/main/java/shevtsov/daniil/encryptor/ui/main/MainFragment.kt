package shevtsov.daniil.encryptor.ui.main

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.content.ContextCompat.checkSelfPermission
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import shevtsov.daniil.encryptor.R
import shevtsov.daniil.encryptor.android.MyApplication
import shevtsov.daniil.encryptor.databinding.FragmentMainBinding
import javax.inject.Inject

class MainFragment : Fragment() {
    companion object {
        private const val READ_PERMISSION_REQUEST_CODE = 1
        private const val READ_REQUEST_CODE = 2

        private const val EXTERNAL_STORAGE_PERMISSION = Manifest.permission.READ_EXTERNAL_STORAGE
    }

    @Inject
    lateinit var mainViewModelFactory: ViewModelProvider.Factory

    private val mainViewModel: MainViewModel by viewModels { mainViewModelFactory }

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {

        val myApplication = activity?.application as MyApplication

        myApplication.appComponent.inject(this)

        val binding: FragmentMainBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_main, container, false)
        binding.lifecycleOwner = this

        binding.viewModel = mainViewModel

        return binding.root
    }

    override fun onViewCreated(
            view: View,
            savedInstanceState: Bundle?
    ) {
        observeViewModel()

        super.onViewCreated(view, savedInstanceState)
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        when (requestCode) {
            READ_PERMISSION_REQUEST_CODE -> {
                val isPermissionGranted = isPermissionGranted(grantResults)

                mainViewModel.onPermissionStatusChanged(isPermissionGranted)
                mainViewModel.onChooseFile()
            }
        }

        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        when {
            requestCode == READ_REQUEST_CODE && resultCode == Activity.RESULT_OK -> {
                val fileUri = data?.data
                fileUri?.let { mainViewModel.onFileChosen(fileUri) }
            }
        }
    }

    override fun onPause() {
        super.onPause()

        mainViewModel.onFinishedTyping()
    }

    private fun observeViewModel() {
        mainViewModel.onPermissionStatusChanged(hasExternalStoragePermission())

        mainViewModel.navigateToEncryptionEvent.observe(viewLifecycleOwner, Observer { plainText ->
            val directions = MainFragmentDirections.navigateToEncryption(sourceText = plainText)
            findNavController().navigate(directions)
        })

        mainViewModel.navigateToDecryptionEvent.observe(viewLifecycleOwner, Observer { plainText ->
            val directions = MainFragmentDirections.navigateToDecryption(sourceText = plainText)
            findNavController().navigate(directions)
        })

        mainViewModel.chooseFileEvent.observe(viewLifecycleOwner, Observer {
            performFileSearch()
        })

        mainViewModel.requestPermissionEvent.observe(viewLifecycleOwner, Observer {
            requestPermissions(
                    arrayOf(EXTERNAL_STORAGE_PERMISSION),
                    READ_PERMISSION_REQUEST_CODE
            )
        })

        mainViewModel.invalidFileExtensionEvent.observe(viewLifecycleOwner, Observer {
            val errorMessage = resources.getString(R.string.invalid_file_extension_error_message)
            Toast.makeText(activity, errorMessage, Toast.LENGTH_SHORT).show()
        })
    }

    private fun performFileSearch() {
        val intent = Intent(Intent.ACTION_GET_CONTENT)
        intent.addCategory(Intent.CATEGORY_OPENABLE)
        intent.type = "text/plain"
        startActivityForResult(intent, READ_REQUEST_CODE)
    }

    private fun isPermissionGranted(grantResults: IntArray) = grantResults.isNotEmpty()
            && grantResults.first() == PackageManager.PERMISSION_GRANTED

    private fun hasExternalStoragePermission(): Boolean = context?.let {
        checkSelfPermission(it, EXTERNAL_STORAGE_PERMISSION) == PackageManager.PERMISSION_GRANTED
    } != null ?: false

}
