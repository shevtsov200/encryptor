package shevtsov.daniil.encryptor.ui.main

import android.net.Uri
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import shevtsov.daniil.encryptor.domain.main.LoadSourceTextUseCase
import shevtsov.daniil.encryptor.domain.main.SaveSourceTextUseCase
import shevtsov.daniil.encryptor.service.repository.localstorage.LocalStorageRepository
import shevtsov.daniil.encryptor.util.Logger
import shevtsov.daniil.encryptor.util.SingleLiveEvent
import shevtsov.daniil.encryptor.util.toImmutable
import shevtsov.daniil.encryptor.util.validation.FileExtensionValidator
import shevtsov.daniil.encryptor.util.validation.MaxTextLengthProvider
import shevtsov.daniil.encryptor.util.validation.SourceTextValidator
import javax.inject.Inject

class MainViewModel @Inject constructor(
        private val logger: Logger,
        private val storeRepository: LocalStorageRepository,
        private val sourceTextValidator: SourceTextValidator,
        private val maxTextLengthProvider: MaxTextLengthProvider,
        private val fileExtensionValidator: FileExtensionValidator,
        private val saveSourceTextUseCase: SaveSourceTextUseCase,
        private val loadSourceTextUseCase: LoadSourceTextUseCase
) : ViewModel() {

    companion object {
        private val TAG = MainViewModel::class.java.simpleName

        private const val TEXT_FILE_EXTENSION = ".txt"
    }

    private val _mainViewState = MutableLiveData<MainViewState>()
    val mainViewState = _mainViewState.toImmutable()

    private val _navigateToEncryptionEvent = SingleLiveEvent<String>()
    val navigateToEncryptionEvent = _navigateToEncryptionEvent.toImmutable()

    private val _navigateToDecryptionEvent = SingleLiveEvent<String>()
    val navigateToDecryptionEvent = _navigateToDecryptionEvent.toImmutable()

    private val _chooseFileEvent = SingleLiveEvent<Void>()
    val chooseFileEvent = _chooseFileEvent.toImmutable()

    private val _requestPermissionEvent = SingleLiveEvent<Void>()
    val requestPermissionEvent = _requestPermissionEvent.toImmutable()

    private val _invalidFileExtensionEvent = SingleLiveEvent<Void>()
    val invalidFileExtensionEvent = _invalidFileExtensionEvent.toImmutable()

    private var isFilePermissionGranted: Boolean = false

    init {
        val savedText = loadSourceTextUseCase.invoke()

        val viewState = MainViewState(
                sourceText = savedText,
                characterCount = 0,
                maxCharacters = maxTextLengthProvider.getAllowedMaxLength(),
                isValidForEncryption = validateForEncryption(savedText),
                isValidForDecryption = validateForDecryption(savedText)
        )

        _mainViewState.value = viewState
    }

    fun onInputChange(inputText: String) {
        _mainViewState.value = _mainViewState.value?.copy(
                sourceText = inputText,
                characterCount = inputText.length,
                isValidForEncryption = validateForEncryption(inputText),
                isValidForDecryption = validateForDecryption(inputText)
        )
    }

    fun onFinishedTyping() {
        val sourceText = _mainViewState.value?.sourceText//_sourceText.value

        sourceText?.let(saveSourceTextUseCase::invoke)
    }

    fun onEncryptionChosen() {
        val sourceText = _mainViewState.value?.sourceText//_sourceText.value

        sourceText?.let {
            _navigateToEncryptionEvent.value = sourceText
        }
    }

    fun onDecryptionChosen() {
        val sourceText = _mainViewState.value?.sourceText//_sourceText.value

        sourceText?.let {
            _navigateToDecryptionEvent.value = sourceText
        }
    }

    fun onChooseFile() {
        if (isFilePermissionGranted) {
            _chooseFileEvent.call()
        } else {
            _requestPermissionEvent.call()
        }
    }

    fun onPermissionStatusChanged(isPermissionGranted: Boolean) {
        isFilePermissionGranted = isPermissionGranted
    }

    fun onFileChosen(fileUri: Uri) {
        if (validateFileExtension(fileUri)) {
            val sourceText = storeRepository.loadText(fileUri)
            onInputChange(sourceText)
        } else {
            _invalidFileExtensionEvent.call()
        }
    }

    private fun validateFileExtension(fileUri: Uri) = fileExtensionValidator.validate(
            fileUri = fileUri,
            allowedExtension = TEXT_FILE_EXTENSION
    )

    private fun validateForEncryption(text: String) = sourceTextValidator.validateTextForEncryption(
            text, maxTextLengthProvider.getAllowedMaxLength()
    )

    private fun validateForDecryption(text: String) = sourceTextValidator.validateTextForDecryption(
            text,
            maxTextLengthProvider.getAllowedMaxLength()
    )

}
