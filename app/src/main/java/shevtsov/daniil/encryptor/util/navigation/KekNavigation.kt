package shevtsov.daniil.encryptor.util.navigation

import androidx.fragment.app.DialogFragment
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

class KekNavigationViewModel : ViewModel() {
    val result = MutableLiveData<String>()

    fun provideKey(resultText: String) {
        result.postValue(resultText)
    }
}

object KekNavigation {
    private const val DIALOG_TAG = "KEK_NAVIGATION_DIALOG"

    fun showDialog(
            activity: FragmentActivity,
            dialog: Class<out DialogFragment>,
            onResultAction: (String) -> Unit
    ) {
        val viewModel = ViewModelProvider(activity).get(KekNavigationViewModel::class.java)
        viewModel.result.observe(activity, Observer { result -> onResultAction.invoke(result) })

        dialog.newInstance().show(activity.supportFragmentManager, DIALOG_TAG)
    }

    fun finishWithResult(
            dialog: DialogFragment,
            result: String
    ) {
        val parentActivity = dialog.activity
        parentActivity?.let {
            val viewModel = ViewModelProvider(parentActivity).get(KekNavigationViewModel::class.java)
            viewModel.provideKey(result)
        }
    }

}