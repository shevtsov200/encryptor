package shevtsov.daniil.encryptor.util.validation

import android.net.Uri
import javax.inject.Inject

//TODO: Temporary until clean architecture migration
class FileExtensionValidator @Inject constructor() {
    fun validate(fileUri: Uri, allowedExtension: String): Boolean {
        val path = fileUri.path

        val extension = path?.substring(path.lastIndexOf("."))

        return extension == allowedExtension
    }
}