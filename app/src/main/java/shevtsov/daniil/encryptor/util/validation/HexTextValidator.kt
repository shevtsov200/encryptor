package shevtsov.daniil.encryptor.util.validation

class HexTextValidator {

    fun validateTextIsHex(text: String): Boolean {
        return text.matches(HEX_REGEX.toRegex())
    }

    companion object {
        private const val HEX_REGEX = "[0-9a-fA-F]+"
    }
}
