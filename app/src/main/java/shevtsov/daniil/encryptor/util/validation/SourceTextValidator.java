package shevtsov.daniil.encryptor.util.validation;

public class SourceTextValidator {
    private final HexTextValidator mHexTextValidator;

    public SourceTextValidator(HexTextValidator hexTextValidator) {
        mHexTextValidator = hexTextValidator;
    }

    public boolean validateTextForEncryption(String sourceText, int maxCharacters) {
        return validateSourceText(sourceText, maxCharacters);
    }

    public boolean validateTextForDecryption(String sourceText, int maxCharacters) {
        return validateSourceText(sourceText, maxCharacters)
                && mHexTextValidator.validateTextIsHex(sourceText);
    }

    private boolean validateSourceText(String sourceText, int maxCharacters) {
        return sourceText != null && !sourceText.isEmpty()
                && sourceText.length() <= maxCharacters;
    }
}
