package shevtsov.daniil.encryptor.util.validation;

public class FileNameValidator {
    public boolean validateFileName(String fileName) {
        return fileName != null && !fileName.isEmpty();
    }

}
