package shevtsov.daniil.encryptor.util.validation

import androidx.annotation.VisibleForTesting
import javax.inject.Inject

//TODO: Temporary until migration to clean architecture
class MaxTextLengthProvider @Inject constructor() {
    companion object {
        @VisibleForTesting
        const val MAX_ALLOWED_TEXT_LENGTH = 50_000
    }

    fun getAllowedMaxLength() = MAX_ALLOWED_TEXT_LENGTH
}