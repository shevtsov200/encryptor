package shevtsov.daniil.encryptor.service.model.encryptor;

import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.security.GeneralSecurityException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import javax.inject.Inject;

import androidx.annotation.NonNull;
import shevtsov.daniil.encryptor.util.ByteHexConverter;
import shevtsov.daniil.encryptor.util.Logger;

public class CipherProvider {
    private static final String TAG = CipherProvider.class.getSimpleName();

    @NonNull
    private final ByteHexConverter mByteHexConverter;

    @NonNull
    private final Logger mLogger;

    @Inject
    public CipherProvider(@NonNull ByteHexConverter byteHexConverter, @NonNull Logger logger) {

        mByteHexConverter = byteHexConverter;
        mLogger = logger;
    }

    public String encrypt(String plainText, String fullAlgorithmName, SecretKey secretKey)
            throws GeneralSecurityException, UnsupportedEncodingException {

            Cipher cipher = Cipher.getInstance(fullAlgorithmName);
            cipher.init(Cipher.ENCRYPT_MODE, secretKey);
            byte[] input = plainText.getBytes(StandardCharsets.UTF_8);

            byte[] encoded = cipher.doFinal(input);

            return mByteHexConverter.bytesToHex(encoded);
    }

    public String decrypt(String cipherText, String algorithmName, String algorithmWithSettings, String key)
            throws NoSuchAlgorithmException, InvalidKeyException {

        try {
            Cipher cipher = Cipher.getInstance(algorithmWithSettings);
            byte[] encodedKey = mByteHexConverter.hexToByte(key);

            SecretKey secretKeySpec = new SecretKeySpec(encodedKey, algorithmName);
            cipher.init(Cipher.DECRYPT_MODE, secretKeySpec);
            byte[] utf8 = mByteHexConverter.hexToByte(cipherText);
            byte[] decodedText = cipher.doFinal(utf8);

            return new String(decodedText);
        } catch (NoSuchPaddingException | BadPaddingException | IllegalBlockSizeException e) {
            mLogger.e(TAG, "decrypt: " + e.getMessage());
        }

        return null;
    }
}
