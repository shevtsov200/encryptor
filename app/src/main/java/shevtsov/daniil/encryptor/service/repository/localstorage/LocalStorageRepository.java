package shevtsov.daniil.encryptor.service.repository.localstorage;

import android.net.Uri;

public interface LocalStorageRepository {
    String loadText(Uri fileUri);

    void saveText(String fileName, String text);
}
