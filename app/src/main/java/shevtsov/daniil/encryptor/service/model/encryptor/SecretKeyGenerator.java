package shevtsov.daniil.encryptor.service.model.encryptor;

import java.security.NoSuchAlgorithmException;

import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.inject.Inject;

import androidx.annotation.NonNull;
import shevtsov.daniil.encryptor.util.ByteHexConverter;

public class SecretKeyGenerator {

    @NonNull
    private final ByteHexConverter mByteHexConverter;

    private SecretKey mSecretKey;

    @Inject
    public SecretKeyGenerator(@NonNull ByteHexConverter byteHexConverter) {
        mByteHexConverter = byteHexConverter;
    }

    void generateKey(String algorithmName) throws NoSuchAlgorithmException {
            mSecretKey = generateSecretKey(algorithmName);
    }

    String getSecretKeyEncoded() {
        return mByteHexConverter.bytesToHex(mSecretKey.getEncoded());
    }

    SecretKey getSecretKey() {
        return mSecretKey;
    }

    private SecretKey generateSecretKey(String algorithmName)
            throws NoSuchAlgorithmException {
        KeyGenerator keyGenerator = KeyGenerator.getInstance(algorithmName);

        return keyGenerator.generateKey();
    }


}
