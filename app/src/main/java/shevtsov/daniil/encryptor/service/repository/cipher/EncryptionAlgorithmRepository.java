package shevtsov.daniil.encryptor.service.repository.cipher;


import java.util.List;

import shevtsov.daniil.encryptor.service.model.AlgorithmModel;

public interface EncryptionAlgorithmRepository {
    List<AlgorithmModel> getAlgorithmList();
}
