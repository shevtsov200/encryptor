package shevtsov.daniil.encryptor.service.model.encryptor;

import javax.inject.Inject;

import shevtsov.daniil.encryptor.service.repository.EncryptionAlgorithmType;

public class EncryptionAlgorithmProvider {
    private static final String ECB_BLOCK_MODE = "ECB";

    private static final String ISO_PADDING = "ISO10126Padding";

    private static final String ALGORITHM_PARAMETERS_DELIMITER = "/";

    @Inject
    public EncryptionAlgorithmProvider() {
        // Required by Dagger2
    }

    private String getBlockMode() {
        return ECB_BLOCK_MODE;
    }

    private String getPaddingType() {
        return ISO_PADDING;
    }

    String getAlgorithmWithSettings(EncryptionAlgorithmType encryptionAlgorithmType) {
        return encryptionAlgorithmType.getStandardAlgorithmName() + ALGORITHM_PARAMETERS_DELIMITER
                + getBlockMode() + ALGORITHM_PARAMETERS_DELIMITER
                + getPaddingType();
    }
}
