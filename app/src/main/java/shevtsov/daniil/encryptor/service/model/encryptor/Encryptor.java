package shevtsov.daniil.encryptor.service.model.encryptor;

import androidx.annotation.NonNull;

import java.io.UnsupportedEncodingException;
import java.security.GeneralSecurityException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import javax.inject.Inject;

import shevtsov.daniil.encryptor.service.repository.EncryptionAlgorithmType;
import shevtsov.daniil.encryptor.util.Logger;

public class Encryptor {
    private static final String TAG = Encryptor.class.getSimpleName();

    private final Logger mLogger;
    private final CipherProvider mCipherProvider;
    private final SecretKeyGenerator mSecretKeyGenerator;
    private final EncryptionAlgorithmProvider mEncryptionAlgorithmProvider;

    private String mAlgorithmName;
    private String mAlgorithmNameWithSettings;

    @Inject
    public Encryptor(Logger logger, CipherProvider cipherProvider,
                     SecretKeyGenerator secretKeyGenerator, EncryptionAlgorithmProvider encryptionAlgorithmProvider) {
        mLogger = logger;
        mCipherProvider = cipherProvider;
        mSecretKeyGenerator = secretKeyGenerator;
        mEncryptionAlgorithmProvider = encryptionAlgorithmProvider;
    }

    public void setAlgorithm(EncryptionAlgorithmType encryptionAlgorithmType) {
        mAlgorithmName = encryptionAlgorithmType.getStandardAlgorithmName();
        mAlgorithmNameWithSettings = mEncryptionAlgorithmProvider.getAlgorithmWithSettings(encryptionAlgorithmType);

        generateKey();
    }

    public String encryptText(@NonNull String plainText) throws GeneralSecurityException, UnsupportedEncodingException {
        return mCipherProvider.encrypt(plainText, mAlgorithmNameWithSettings, mSecretKeyGenerator.getSecretKey());
    }

    public String decipherText(String cipherText, String key) throws InvalidKeyException, NoSuchAlgorithmException {
        return mCipherProvider.decrypt(cipherText, mAlgorithmName, mAlgorithmNameWithSettings, key);
    }

    public String getKey() {
        return mSecretKeyGenerator.getSecretKeyEncoded();
    }

    private void generateKey() {
        try {
            mSecretKeyGenerator.generateKey(mAlgorithmName);
        } catch (NoSuchAlgorithmException e) {
            mLogger.e(TAG, "generateKey: " + e.getMessage());
        }
    }
}