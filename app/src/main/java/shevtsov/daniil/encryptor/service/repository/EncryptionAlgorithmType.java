package shevtsov.daniil.encryptor.service.repository;

public enum EncryptionAlgorithmType {
    DES("DES"),
    BLOWFISH("BLOWFISH"),
    AES("AES"),
    DES_EDE("DESede");

    private final String mStandardAlgorithmName;

    EncryptionAlgorithmType(String standardAlgorithmName) {
        mStandardAlgorithmName = standardAlgorithmName;
    }

    public String getStandardAlgorithmName() {
        return mStandardAlgorithmName;
    }
}
