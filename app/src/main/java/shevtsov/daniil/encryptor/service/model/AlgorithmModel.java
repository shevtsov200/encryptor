package shevtsov.daniil.encryptor.service.model;

import shevtsov.daniil.encryptor.service.repository.EncryptionAlgorithmType;

public class AlgorithmModel {
    private final EncryptionAlgorithmType mAlgorithmType;
    private final String mEncryptionAlgorithmName;

    public AlgorithmModel(EncryptionAlgorithmType algorithmType, String encryptionAlgorithmName) {
        mAlgorithmType = algorithmType;
        mEncryptionAlgorithmName = encryptionAlgorithmName;
    }

    public String getEncryptionAlgorithmName() {
        return mEncryptionAlgorithmName;
    }

    public EncryptionAlgorithmType getAlgorithmType() {
        return mAlgorithmType;
    }
}
