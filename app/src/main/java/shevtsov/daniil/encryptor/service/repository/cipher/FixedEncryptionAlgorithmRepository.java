package shevtsov.daniil.encryptor.service.repository.cipher;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.inject.Inject;

import shevtsov.daniil.encryptor.service.model.AlgorithmModel;
import shevtsov.daniil.encryptor.service.repository.EncryptionAlgorithmType;

public class FixedEncryptionAlgorithmRepository implements EncryptionAlgorithmRepository {

    @Inject
    public FixedEncryptionAlgorithmRepository() {
        // Required by Dagger2
    }

    @Override
    public List<AlgorithmModel> getAlgorithmList() {
        return getCiphers();
    }

    private List<AlgorithmModel> getCiphers() {
        List<EncryptionAlgorithmType> encryptionAlgorithmTypes = Arrays.asList(EncryptionAlgorithmType.values());
        List<AlgorithmModel> algorithmModelList = new ArrayList<>();

        for (EncryptionAlgorithmType encryptionAlgorithmType : encryptionAlgorithmTypes) {
            AlgorithmModel algorithmModel = new AlgorithmModel(encryptionAlgorithmType,
                    encryptionAlgorithmType.getStandardAlgorithmName());
            algorithmModelList.add(algorithmModel);
        }
        return algorithmModelList;
    }
}
