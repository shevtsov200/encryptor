package shevtsov.daniil.encryptor.domain.main

import shevtsov.daniil.encryptor.data.repository.SourceTextRepository
import javax.inject.Inject

class SaveSourceTextUseCase @Inject constructor(
        private val sourceTextRepository: SourceTextRepository
) {

    operator fun invoke(sourceText: String) = sourceTextRepository.save(sourceText)

}
