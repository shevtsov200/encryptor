package shevtsov.daniil.encryptor.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import shevtsov.daniil.encryptor.service.model.AlgorithmModel
import shevtsov.daniil.encryptor.service.repository.EncryptionAlgorithmType
import shevtsov.daniil.encryptor.service.repository.cipher.EncryptionAlgorithmRepository
import javax.inject.Inject

class AlgorithmListViewModel @Inject constructor(
        encryptionAlgorithmRepository: EncryptionAlgorithmRepository
) : ViewModel() {

    val selectedAlgorithm: LiveData<EncryptionAlgorithmType> get() = _selectedAlgorithm
    private val _selectedAlgorithm = MutableLiveData<EncryptionAlgorithmType>()

    val algorithmsList: LiveData<List<AlgorithmModel>> get() = _algorithmsList
    private val _algorithmsList = MutableLiveData<List<AlgorithmModel>>()

    init {
        val algorithms = encryptionAlgorithmRepository.algorithmList
        _algorithmsList.value = algorithms
    }

    fun onAlgorithmChosen(encryptionAlgorithmType: EncryptionAlgorithmType) {
        _selectedAlgorithm.value = encryptionAlgorithmType
    }

}
