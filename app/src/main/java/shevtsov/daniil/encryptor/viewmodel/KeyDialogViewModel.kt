package shevtsov.daniil.encryptor.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import shevtsov.daniil.encryptor.util.Logger
import shevtsov.daniil.encryptor.util.SingleLiveEvent
import shevtsov.daniil.encryptor.util.validation.HexTextValidator
import javax.inject.Inject

class KeyDialogViewModel @Inject constructor(
        private val logger: Logger,
        private val hexTextValidator: HexTextValidator
) : ViewModel() {
    companion object {
        private val TAG = KeyDialogViewModel::class.java.simpleName
    }

    val provideKeyEvent = SingleLiveEvent<String>()

    val isKeyValid: LiveData<Boolean>
        get() = _isKeyValid
    private val _isKeyValid = MutableLiveData<Boolean>()

    private var decryptionKey: String? = null

    fun onKeyProvided() {
        provideKeyEvent.value = decryptionKey
    }

    fun onInputChange(keyText: String) {
        decryptionKey = keyText

        val isKeyValid = hexTextValidator.validateTextIsHex(keyText)
        _isKeyValid.value = isKeyValid
    }
}