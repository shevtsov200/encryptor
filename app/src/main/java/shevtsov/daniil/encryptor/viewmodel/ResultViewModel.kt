package shevtsov.daniil.encryptor.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import shevtsov.daniil.encryptor.service.repository.localstorage.LocalStorageRepository
import shevtsov.daniil.encryptor.util.Logger
import shevtsov.daniil.encryptor.util.SingleLiveEvent
import shevtsov.daniil.encryptor.util.android.ClipboardRepository
import javax.inject.Inject

class ResultViewModel @Inject constructor(
        private val logger: Logger,
        private val localStorageRepository: LocalStorageRepository,
        private val clipboardRepsitory: ClipboardRepository
) : ViewModel() {

    companion object {
        private val TAG = ResultViewModel::class.java.simpleName
    }

    private val _resultText = MutableLiveData<String>()
    val resultText: LiveData<String>
        get() = _resultText

    private val _encryptionKey = MutableLiveData<String>()
    val encryptionKey: LiveData<String>
        get() = _encryptionKey

    private val _hasKey = MutableLiveData<Boolean>()
    val hasKey: LiveData<Boolean>
        get() = _hasKey

    val shareTextEvent = SingleLiveEvent<String>()
    val chooseFileEvent = SingleLiveEvent<Void>()

    init {
        _hasKey.value = false
    }

    fun setResultText(resultText: String) {
        _resultText.value = resultText
    }

    fun setEncryptionKey(encryptionKey: String) {
        _encryptionKey.value = encryptionKey
        _hasKey.value = true
    }

    fun onSaveText() {
        chooseFileEvent.call()
    }

    fun onCopyText() {
        val resultText = _resultText.value
        resultText?.let { copyToClipboard(resultText) }
    }

    fun onCopyKey() {
        val encryptionKey = _encryptionKey.value
        encryptionKey?.let { copyToClipboard(encryptionKey) }
    }

    fun onShareText() {
        val resultText = _resultText.value

        shareTextEvent.value = resultText
    }

    private fun copyToClipboard(text: String) {
        clipboardRepsitory.copyToClipboard(text)
    }

    fun onFileNameProvided(fileName: String) {
        val resultText = _resultText.value

        localStorageRepository.saveText(fileName, resultText)
    }

}
