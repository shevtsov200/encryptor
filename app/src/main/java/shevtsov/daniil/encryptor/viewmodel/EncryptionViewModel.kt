package shevtsov.daniil.encryptor.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import shevtsov.daniil.encryptor.R
import shevtsov.daniil.encryptor.android.model.CipherResult
import shevtsov.daniil.encryptor.service.model.AlgorithmModel
import shevtsov.daniil.encryptor.service.model.encryptor.Encryptor
import shevtsov.daniil.encryptor.service.repository.EncryptionAlgorithmType
import shevtsov.daniil.encryptor.service.repository.cipher.EncryptionAlgorithmRepository
import shevtsov.daniil.encryptor.util.Logger
import shevtsov.daniil.encryptor.util.SingleLiveEvent
import java.io.UnsupportedEncodingException
import java.security.GeneralSecurityException
import java.security.InvalidKeyException
import java.security.NoSuchAlgorithmException
import javax.inject.Inject

class EncryptionViewModel @Inject constructor(
        private val logger: Logger,
        private val encryptor: Encryptor,
        encryptionAlgorithmRepository: EncryptionAlgorithmRepository
) : ViewModel() {

    companion object {
        private val TAG = EncryptionViewModel::class.java.simpleName
    }

    val navigateToResultEvent = SingleLiveEvent<CipherResult>()

    val algorithmsList: LiveData<List<AlgorithmModel>> get() = _algorithmsList
    private val _algorithmsList = MutableLiveData<List<AlgorithmModel>>()

    init {
        val algorithms = encryptionAlgorithmRepository.algorithmList
        _algorithmsList.value = algorithms
    }

    val showErrorMessageEvent = SingleLiveEvent<Int>()

    private var plainText: String? = null

    fun onAlgorithmChosen(encryptionAlgorithmType: EncryptionAlgorithmType) {
        encryptor.setAlgorithm(encryptionAlgorithmType)

        encryptText()
    }

    fun onPlainTextProvided(plainText: String) {
        this.plainText = plainText
    }

    private fun encryptText() {
        plainText?.let { plainText ->
            try {
                val encryptedText = encryptor.encryptText(plainText)
                val encryptionKey = encryptor.key

                val encryptionResult = CipherResult(
                       resultText =  encryptedText,
                       encryptionKey =  encryptionKey
                )

                navigateToResultEvent.postValue(encryptionResult)
            } catch (e: InvalidKeyException) {
                logger.e(TAG, "InvalidKeyException: " + e.message)
                showErrorMessageEvent.value = R.string.invalid_encryption_key_message
            } catch (e: NoSuchAlgorithmException) {
                logger.e(TAG, "NoSuchAlgorithmException: " + e.message)
                showErrorMessageEvent.value = R.string.invalid_algorithm_name_message
            } catch (e: UnsupportedEncodingException) {
                logger.e(TAG, "UnsupportedEncodingException: " + e.message)
                showErrorMessageEvent.value = R.string.invalid_text_encoding_message
            } catch (e: GeneralSecurityException) {
                logger.e(TAG, "GeneralSecurityException: " + e.message)
                showErrorMessageEvent.value = R.string.encryption_error_message
            }
        }
    }

}
