package shevtsov.daniil.encryptor.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import shevtsov.daniil.encryptor.R
import shevtsov.daniil.encryptor.android.model.CipherResult
import shevtsov.daniil.encryptor.service.model.AlgorithmModel
import shevtsov.daniil.encryptor.service.model.encryptor.Encryptor
import shevtsov.daniil.encryptor.service.repository.EncryptionAlgorithmType
import shevtsov.daniil.encryptor.service.repository.cipher.EncryptionAlgorithmRepository
import shevtsov.daniil.encryptor.util.Logger
import shevtsov.daniil.encryptor.util.SingleLiveEvent
import java.security.InvalidKeyException
import java.security.NoSuchAlgorithmException
import javax.inject.Inject

class DecryptionViewModel @Inject constructor(
        private val logger: Logger,
        private val encryptor: Encryptor,
        encryptionAlgorithmRepository: EncryptionAlgorithmRepository
) : ViewModel() {

    companion object {
        private val TAG = DecryptionViewModel::class.java.simpleName
    }

    val requestKeyEvent = SingleLiveEvent<Void>()
    val showErrorMessageEvent = SingleLiveEvent<Int>()

    private var encryptedText: String? = null

    val navigateToResultEvent = SingleLiveEvent<CipherResult>()

    val algorithmsList: LiveData<List<AlgorithmModel>> get() = _algorithmsList
    private val _algorithmsList = MutableLiveData<List<AlgorithmModel>>()

    init {
        val algorithms = encryptionAlgorithmRepository.algorithmList
        _algorithmsList.value = algorithms
    }

    fun onKeyProvided(decryptionKey: String) {
        try {
            val decryptedText = encryptor.decipherText(encryptedText, decryptionKey)
            val cipherResult = CipherResult(resultText = decryptedText)
            navigateToResultEvent.postValue(cipherResult)
        } catch (e: InvalidKeyException) {
            logger.e(TAG, "invalid key: " + e.message)
            showErrorMessageEvent.value = R.string.invalid_decryption_key_message
        } catch (e: NoSuchAlgorithmException) {
            logger.e(TAG, "invalid algorithm: " + e.message)
            showErrorMessageEvent.value = R.string.invalid_algorithm_name_message
        } catch (e: Throwable) {
            logger.e(TAG, "unhandled exception: ${e.message}")
        }

    }

    //TODO: Fix this temporal coupling.
    // onKeyProvided must be called after onAlgorithmChosen but it is not enforced in any way
    fun onAlgorithmChosen(encryptionAlgorithmType: EncryptionAlgorithmType) {
        requestKeyEvent.call()

        encryptor.setAlgorithm(encryptionAlgorithmType)
    }

    fun onEncryptedTextProvided(newEncryptedText: String) {
        encryptedText = newEncryptedText
    }
}
