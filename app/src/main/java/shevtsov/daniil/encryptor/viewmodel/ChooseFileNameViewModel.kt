package shevtsov.daniil.encryptor.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import shevtsov.daniil.encryptor.util.Logger
import shevtsov.daniil.encryptor.util.SingleLiveEvent
import shevtsov.daniil.encryptor.util.validation.FileNameValidator
import javax.inject.Inject

class ChooseFileNameViewModel @Inject constructor(
        private val logger: Logger,
        private val fileNameValidator: FileNameValidator
) : ViewModel() {
    companion object {
        private val TAG = ChooseFileNameViewModel::class.java.simpleName
    }

    private val _isFileNameValid = MutableLiveData<Boolean>()
    val isFileNameValid: LiveData<Boolean>
        get() = _isFileNameValid

    private var fileName: String? = null

    val provideFileNameEvent = SingleLiveEvent<String>()

    fun returnFileNameFromDialog() {
        provideFileNameEvent.value = fileName
    }

    fun onInputChange(newFileName: String) {
        fileName = newFileName

        val isValid = fileNameValidator.validateFileName(fileName)
        _isFileNameValid.value = isValid
    }
}
