package shevtsov.daniil.encryptor.android;

import android.app.Application;

import shevtsov.daniil.encryptor.dagger.component.AppComponent;
import shevtsov.daniil.encryptor.dagger.component.DaggerAppComponent;
import shevtsov.daniil.encryptor.dagger.module.AppModule;

public class MyApplication extends Application {
    private AppComponent mAppComponent;

    @Override
    public void onCreate() {
        super.onCreate();

        mAppComponent = DaggerAppComponent.builder()
                .appModule(new AppModule(getApplicationContext()))
                .build();
    }

    public AppComponent getAppComponent() {
        return mAppComponent;
    }
}
