package shevtsov.daniil.encryptor.android.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class CipherResult(
        val resultText: String,
        val encryptionKey: String? = null
) : Parcelable