package shevtsov.daniil.encryptor.android.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class DecryptionResult(
        val decryptedText: String
) : Parcelable