package shevtsov.daniil.encryptor.data.repository

import shevtsov.daniil.encryptor.util.android.SharedPreferencesManager
import javax.inject.Inject

class SourceTextRepository @Inject constructor(
        private val sharedPreferencesManager: SharedPreferencesManager
) {

    companion object {
        private const val SOURCE_TEXT_PREFERENCE_KEY = "source-text-preference"
    }

    fun save(sourceText: String) = sharedPreferencesManager.storeString(SOURCE_TEXT_PREFERENCE_KEY, sourceText)

    fun load(): String = sharedPreferencesManager.getString(SOURCE_TEXT_PREFERENCE_KEY, "")

}
