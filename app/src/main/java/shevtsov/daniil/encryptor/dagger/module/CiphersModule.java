package shevtsov.daniil.encryptor.dagger.module;

import dagger.Module;
import dagger.Provides;
import shevtsov.daniil.encryptor.service.repository.cipher.EncryptionAlgorithmRepository;
import shevtsov.daniil.encryptor.service.repository.cipher.FixedEncryptionAlgorithmRepository;

@Module
public class CiphersModule {
    @Provides
    public EncryptionAlgorithmRepository provideCiphersRepository() {
        return new FixedEncryptionAlgorithmRepository();
    }
}
