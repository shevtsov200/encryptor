package shevtsov.daniil.encryptor.dagger.component;

import org.jetbrains.annotations.NotNull;

import dagger.Component;
import shevtsov.daniil.encryptor.dagger.module.AppModule;
import shevtsov.daniil.encryptor.dagger.scope.ApplicationScope;
import shevtsov.daniil.encryptor.ui.main.MainFragment;
import shevtsov.daniil.encryptor.view.ui.dialog.ChooseFileNameDialog;
import shevtsov.daniil.encryptor.view.ui.dialog.DecryptionKeyDialog;
import shevtsov.daniil.encryptor.view.ui.fragment.AlgorithmListFragment;
import shevtsov.daniil.encryptor.view.ui.fragment.DecryptionFragment;
import shevtsov.daniil.encryptor.view.ui.fragment.EncryptionFragment;
import shevtsov.daniil.encryptor.view.ui.fragment.ResultFragment;

@ApplicationScope
@Component(modules = {AppModule.class})
public interface AppComponent {

    void inject(ChooseFileNameDialog chooseFileNameDialog);

    void inject(DecryptionKeyDialog decryptionKeyDialog);

    void inject(AlgorithmListFragment cipherListFragment);

    void inject(MainFragment mainFragment);

    void inject(EncryptionFragment encryptionFragment);

    void inject(ResultFragment resultFragment);

    void inject(@NotNull final DecryptionFragment decryptionFragment);
}
