package shevtsov.daniil.encryptor.dagger.module;

import android.content.Context;

import dagger.Module;
import dagger.Provides;
import shevtsov.daniil.encryptor.service.repository.localstorage.FileRepository;
import shevtsov.daniil.encryptor.service.repository.localstorage.LocalStorageRepository;
import shevtsov.daniil.encryptor.util.Logger;

@Module
public class StorageModule {
    @Provides
    LocalStorageRepository provideLocalStorageRepository(Logger logger, Context context) {
        return new FileRepository(logger, context);
    }
}
