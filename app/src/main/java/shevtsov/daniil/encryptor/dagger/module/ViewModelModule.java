package shevtsov.daniil.encryptor.dagger.module;

import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import dagger.Binds;
import dagger.Module;
import dagger.multibindings.IntoMap;
import shevtsov.daniil.encryptor.dagger.viewmodel.DaggerViewModelFactory;
import shevtsov.daniil.encryptor.dagger.viewmodel.ViewModelKey;
import shevtsov.daniil.encryptor.ui.main.MainViewModel;
import shevtsov.daniil.encryptor.viewmodel.AlgorithmListViewModel;
import shevtsov.daniil.encryptor.viewmodel.ChooseFileNameViewModel;
import shevtsov.daniil.encryptor.viewmodel.DecryptionViewModel;
import shevtsov.daniil.encryptor.viewmodel.EncryptionViewModel;
import shevtsov.daniil.encryptor.viewmodel.KeyDialogViewModel;
import shevtsov.daniil.encryptor.viewmodel.ResultViewModel;

@Module
public abstract class ViewModelModule {
    @Binds
    @IntoMap
    @ViewModelKey(ChooseFileNameViewModel.class)
    abstract ViewModel bindChooseFileNameViewModel(ChooseFileNameViewModel chooseFileNameViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(AlgorithmListViewModel.class)
    abstract ViewModel bindCipherListViewModel(AlgorithmListViewModel algorithmListViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(DecryptionViewModel.class)
    abstract ViewModel bindDecryptionViewModel(DecryptionViewModel decryptionViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(EncryptionViewModel.class)
    abstract ViewModel bindViewModel(EncryptionViewModel encryptionViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(KeyDialogViewModel.class)
    abstract ViewModel bindKeyDialogViewModel(KeyDialogViewModel keyDialogViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(MainViewModel.class)
    abstract ViewModel bindMainViewModel(MainViewModel mainViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(ResultViewModel.class)
    abstract ViewModel bindResultViewModel(ResultViewModel resultViewModel);

    @Binds
    abstract ViewModelProvider.Factory bindViewModelFactory(DaggerViewModelFactory factory);
}
