package shevtsov.daniil.encryptor.dagger.module;

import android.content.Context;

import dagger.Module;
import dagger.Provides;
import shevtsov.daniil.encryptor.dagger.scope.ApplicationScope;
import shevtsov.daniil.encryptor.util.Logger;

@Module(includes = {ViewModelModule.class, AndroidModule.class,
        StorageModule.class, ValidationModule.class, EncryptionModule.class,
        CiphersModule.class})
public class AppModule {
    private final Context mContext;

    public AppModule(Context context) {
        mContext = context;
    }

    @Provides
    @ApplicationScope
    Context provideContext() {
        return mContext;
    }

    @Provides
    Logger provideLogger() {
        return new Logger();
    }
}
