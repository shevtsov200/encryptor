package shevtsov.daniil.encryptor.dagger.module;

import dagger.Module;
import dagger.Provides;
import shevtsov.daniil.encryptor.service.model.encryptor.CipherProvider;
import shevtsov.daniil.encryptor.service.model.encryptor.EncryptionAlgorithmProvider;
import shevtsov.daniil.encryptor.service.model.encryptor.Encryptor;
import shevtsov.daniil.encryptor.service.model.encryptor.SecretKeyGenerator;
import shevtsov.daniil.encryptor.util.ByteHexConverter;
import shevtsov.daniil.encryptor.util.Logger;

@Module
public class EncryptionModule {
    @Provides
    public ByteHexConverter provideByteHexConverter() {
        return new ByteHexConverter();
    }

    @Provides
    public CipherProvider provideCipherProvider(ByteHexConverter byteHexConverter, Logger logger) {
        return new CipherProvider(byteHexConverter, logger);
    }

    @Provides
    public SecretKeyGenerator provideSecretKeyGenerator(ByteHexConverter byteHexConverter) {
        return new SecretKeyGenerator(byteHexConverter);
    }

    @Provides
    public Encryptor provideEncryptor(Logger logger, CipherProvider cipherProvider,
                                      SecretKeyGenerator secretKeyGenerator,
                                      EncryptionAlgorithmProvider encryptionAlgorithmProvider) {
        return new Encryptor(logger, cipherProvider, secretKeyGenerator, encryptionAlgorithmProvider);
    }
}
