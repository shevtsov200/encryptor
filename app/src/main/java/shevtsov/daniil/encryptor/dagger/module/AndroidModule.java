package shevtsov.daniil.encryptor.dagger.module;

import android.content.Context;

import dagger.Module;
import dagger.Provides;
import shevtsov.daniil.encryptor.util.android.ClipboardRepository;
import shevtsov.daniil.encryptor.util.android.SharedPreferencesManager;

@Module
public class AndroidModule {
    @Provides
    SharedPreferencesManager provideSharedPreferencesManager(Context context) {
        return new SharedPreferencesManager(context);
    }

    @Provides
    ClipboardRepository provideClipboardRepository(Context context) {
        return new ClipboardRepository(context);
    }
}
