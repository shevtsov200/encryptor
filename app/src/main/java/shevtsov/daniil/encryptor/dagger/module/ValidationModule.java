package shevtsov.daniil.encryptor.dagger.module;

import dagger.Module;
import dagger.Provides;
import shevtsov.daniil.encryptor.util.validation.FileNameValidator;
import shevtsov.daniil.encryptor.util.validation.HexTextValidator;
import shevtsov.daniil.encryptor.util.validation.SourceTextValidator;

@Module
public class ValidationModule {
    @Provides
    FileNameValidator provideFileNameValidator() {
        return new FileNameValidator();
    }

    @Provides
    HexTextValidator provideHexTextValidator() {
        return new HexTextValidator();
    }

    @Provides
    SourceTextValidator provideSourceTextValidator(HexTextValidator hexTextValidator) {
        return new SourceTextValidator(hexTextValidator);
    }
}
