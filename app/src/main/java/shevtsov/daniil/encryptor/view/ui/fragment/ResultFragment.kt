package shevtsov.daniil.encryptor.view.ui.fragment

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.navArgs
import shevtsov.daniil.encryptor.R
import shevtsov.daniil.encryptor.android.MyApplication
import shevtsov.daniil.encryptor.databinding.ActivityResultBinding
import shevtsov.daniil.encryptor.util.navigation.KekNavigation
import shevtsov.daniil.encryptor.view.ui.dialog.ChooseFileNameDialog
import shevtsov.daniil.encryptor.viewmodel.ResultViewModel
import javax.inject.Inject

class ResultFragment : Fragment() {
    private val navigationArguments: ResultFragmentArgs by navArgs()

    @Inject
    lateinit var resultViewModelFactory: ViewModelProvider.Factory

    private val resultViewModel: ResultViewModel by viewModels { resultViewModelFactory }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val myApplication = activity?.application as MyApplication

        myApplication.appComponent.inject(this)

        val binding: ActivityResultBinding = DataBindingUtil.inflate(inflater, R.layout.activity_result, container, false)
        binding.lifecycleOwner = this

        binding.viewModel = resultViewModel

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        observeViewModel()

        val resultText = navigationArguments.cipherResult.resultText
        val encryptionKey = navigationArguments.cipherResult.encryptionKey

        resultViewModel.setResultText(resultText)
        encryptionKey?.let { resultViewModel.setEncryptionKey(encryptionKey) }

        super.onViewCreated(view, savedInstanceState)
    }

    private fun observeViewModel() {
        resultViewModel.shareTextEvent.observe(viewLifecycleOwner, Observer<String> { text ->
            shareText(text)
        })

        resultViewModel.chooseFileEvent.observe(viewLifecycleOwner, Observer { showChooseFileDialog() })
    }

    private fun showChooseFileDialog() {
        activity?.let {
            KekNavigation.showDialog(
                    activity = it,
                    dialog = ChooseFileNameDialog::class.java
            ) { fileName ->
                resultViewModel.onFileNameProvided(fileName)
            }
        }
    }

    private fun shareText(text: String) {
        val shareIntent = Intent()
        shareIntent.action = Intent.ACTION_SEND
        shareIntent.putExtra(Intent.EXTRA_TEXT, text)
        shareIntent.type = "text/plain"
        startActivity(shareIntent)
    }
}