package shevtsov.daniil.encryptor.view.ui.activity

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import shevtsov.daniil.encryptor.R

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_main)
    }

}
