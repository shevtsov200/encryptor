package shevtsov.daniil.encryptor.view.callback;

import shevtsov.daniil.encryptor.service.model.AlgorithmModel;

public interface AlgorithmClickListener {
    void onClick(AlgorithmModel algorithmModel);
}
