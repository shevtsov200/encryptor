package shevtsov.daniil.encryptor.view.ui.dialog

import android.annotation.SuppressLint
import android.app.Dialog
import android.os.Bundle
import android.view.LayoutInflater
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import shevtsov.daniil.encryptor.R
import shevtsov.daniil.encryptor.android.MyApplication
import shevtsov.daniil.encryptor.databinding.DialogChooseFileBinding
import shevtsov.daniil.encryptor.util.navigation.KekNavigation
import shevtsov.daniil.encryptor.viewmodel.ChooseFileNameViewModel
import javax.inject.Inject

class ChooseFileNameDialog : DialogFragment() {
    companion object {
        private val TAG = ChooseFileNameDialog::class.java.name
    }

    @Inject
    lateinit var chooseFileNameViewModelFactory: ViewModelProvider.Factory

    private val chooseFileNameViewModel: ChooseFileNameViewModel by viewModels { chooseFileNameViewModelFactory }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val myApplication = activity?.application as MyApplication

        myApplication.appComponent.inject(this)

        val binding = DataBindingUtil.inflate<DialogChooseFileBinding>(LayoutInflater.from(activity),
                R.layout.dialog_choose_file, null, false)
        binding.lifecycleOwner = this
        binding.viewModel = chooseFileNameViewModel

        return MaterialAlertDialogBuilder(activity)
                .setView(binding.root)
                .create()
    }

    @SuppressLint("FragmentLiveDataObserve")
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        chooseFileNameViewModel.provideFileNameEvent.observe(this, Observer { fileName ->
                KekNavigation.finishWithResult(
                        dialog = this,
                        result = fileName
                )

                dismiss()
        })

    }
}
