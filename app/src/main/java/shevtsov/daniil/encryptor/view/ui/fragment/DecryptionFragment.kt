package shevtsov.daniil.encryptor.view.ui.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.LinearLayoutManager
import shevtsov.daniil.encryptor.R
import shevtsov.daniil.encryptor.android.MyApplication
import shevtsov.daniil.encryptor.databinding.ActivityDecryptionBinding
import shevtsov.daniil.encryptor.service.model.AlgorithmModel
import shevtsov.daniil.encryptor.util.navigation.KekNavigation
import shevtsov.daniil.encryptor.view.adapter.EncryptionAlgorithmListAdapter
import shevtsov.daniil.encryptor.view.callback.AlgorithmClickListener
import shevtsov.daniil.encryptor.view.ui.dialog.DecryptionKeyDialog
import shevtsov.daniil.encryptor.viewmodel.DecryptionViewModel
import javax.inject.Inject

class DecryptionFragment : Fragment(), AlgorithmClickListener {
    private val navigationArguments: DecryptionFragmentArgs by navArgs()

    @Inject
    lateinit var decryptionViewModelFactory: ViewModelProvider.Factory

    private val decryptionViewModel: DecryptionViewModel by viewModels { decryptionViewModelFactory }


    private var algorithmListAdapter = EncryptionAlgorithmListAdapter(this)

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val myApplication = activity?.application as MyApplication

        myApplication.appComponent.inject(this)

        val binding: ActivityDecryptionBinding = DataBindingUtil.inflate(inflater, R.layout.activity_decryption, container, false)

        binding.viewModel = decryptionViewModel

        binding.cipherListView.layoutManager = LinearLayoutManager(activity)
        binding.cipherListView.adapter = algorithmListAdapter

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        observeViewModel()

        val sourceText = navigationArguments.sourceText

        decryptionViewModel.onEncryptedTextProvided(sourceText)

        super.onViewCreated(view, savedInstanceState)
    }

    private fun observeViewModel() {
        decryptionViewModel.algorithmsList.observe(viewLifecycleOwner, Observer { ciphers ->
            algorithmListAdapter.setAlgorithmList(ciphers)
        })

        decryptionViewModel.navigateToResultEvent.observe(viewLifecycleOwner, Observer { decryptionResult ->
            val directions = DecryptionFragmentDirections.navigateFromDecryptionToResult(
                    cipherResult = decryptionResult
            )
            findNavController().navigate(directions)
        })

        decryptionViewModel.requestKeyEvent.observe(viewLifecycleOwner, Observer { showKeyDialog() })

        decryptionViewModel.showErrorMessageEvent.observe(viewLifecycleOwner, Observer { errorStringId ->
            val errorMessage = resources.getString(errorStringId)
            Toast.makeText(activity, errorMessage, Toast.LENGTH_SHORT).show()
        })
    }

    private fun showKeyDialog() {
        activity?.let {
            KekNavigation.showDialog(
                    activity = it,
                    dialog = DecryptionKeyDialog::class.java
            ) { key ->
                onKeyProvided(decryptionKey = key)
            }
        }

    }

    fun onKeyProvided(decryptionKey: String) {
        decryptionViewModel.onKeyProvided(decryptionKey)
    }

    override fun onClick(algorithmModel: AlgorithmModel) {
        decryptionViewModel.onAlgorithmChosen(algorithmModel.algorithmType)
    }
}