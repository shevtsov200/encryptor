package shevtsov.daniil.encryptor.view.callback;

import shevtsov.daniil.encryptor.service.repository.EncryptionAlgorithmType;

public interface EncryptionAlgorithmListener {
    void onEncryptionAlgorithmChosen(EncryptionAlgorithmType algorithmType);
}
