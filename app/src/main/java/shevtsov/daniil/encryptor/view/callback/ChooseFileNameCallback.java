package shevtsov.daniil.encryptor.view.callback;

public interface ChooseFileNameCallback {
    void onFileNameProvided(String fileName);
}
