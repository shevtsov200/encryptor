package shevtsov.daniil.encryptor.view.ui.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.LinearLayoutManager
import shevtsov.daniil.encryptor.R
import shevtsov.daniil.encryptor.android.MyApplication
import shevtsov.daniil.encryptor.databinding.FragmentEncryptionBinding
import shevtsov.daniil.encryptor.service.model.AlgorithmModel
import shevtsov.daniil.encryptor.view.adapter.EncryptionAlgorithmListAdapter
import shevtsov.daniil.encryptor.view.callback.AlgorithmClickListener
import shevtsov.daniil.encryptor.viewmodel.EncryptionViewModel
import javax.inject.Inject

class EncryptionFragment : Fragment(), AlgorithmClickListener {
    private val navigationArguments: EncryptionFragmentArgs by navArgs()

    @Inject
    lateinit var encryptionViewModelFactory: ViewModelProvider.Factory

    private val encryptionViewModel: EncryptionViewModel by viewModels { encryptionViewModelFactory }

    private var algorithmListAdapter = EncryptionAlgorithmListAdapter(this)

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val myApplication = activity?.application as MyApplication

        myApplication.appComponent.inject(this)

        val binding: FragmentEncryptionBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_encryption, container, false)
        binding.lifecycleOwner = this
        binding.viewModel = encryptionViewModel

        binding.cipherListView.layoutManager = LinearLayoutManager(activity)
        binding.cipherListView.adapter = algorithmListAdapter

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        observeViewModel()

        val sourceText = navigationArguments.sourceText
        encryptionViewModel.onPlainTextProvided(plainText = sourceText)

        super.onViewCreated(view, savedInstanceState)
    }

    private fun observeViewModel() {
        encryptionViewModel.algorithmsList.observe(viewLifecycleOwner, Observer { ciphers ->
            algorithmListAdapter.setAlgorithmList(ciphers)
        })

        encryptionViewModel.navigateToResultEvent.observe(viewLifecycleOwner, Observer { encryptionResult ->
            val directions = EncryptionFragmentDirections.navigateFromEncryptionToResult(
                    cipherResult = encryptionResult
            )
            findNavController().navigate(directions)
        })

        encryptionViewModel.showErrorMessageEvent.observe(viewLifecycleOwner, Observer { errorStringId ->
            val errorMessage = resources.getString(errorStringId)
            Toast.makeText(activity, errorMessage, Toast.LENGTH_SHORT).show()
        })
    }

    override fun onClick(algorithmModel: AlgorithmModel) {
        encryptionViewModel.onAlgorithmChosen(algorithmModel.algorithmType)
    }
}