package shevtsov.daniil.encryptor.view.ui.fragment

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import shevtsov.daniil.encryptor.R
import shevtsov.daniil.encryptor.android.MyApplication
import shevtsov.daniil.encryptor.databinding.FragmentAlgorithmsBinding
import shevtsov.daniil.encryptor.service.model.AlgorithmModel
import shevtsov.daniil.encryptor.service.repository.EncryptionAlgorithmType
import shevtsov.daniil.encryptor.util.SingleLiveEvent
import shevtsov.daniil.encryptor.view.adapter.EncryptionAlgorithmListAdapter
import shevtsov.daniil.encryptor.view.callback.AlgorithmClickListener
import shevtsov.daniil.encryptor.viewmodel.AlgorithmListViewModel
import javax.inject.Inject

interface SelectedAlgorithmViewModel {
    val selectedAlgorithmType: SingleLiveEvent<EncryptionAlgorithmType>

    fun onAlgorithmSelected(selectedAlgorithm: EncryptionAlgorithmType) {
        selectedAlgorithmType.postValue(selectedAlgorithm)
    }
}

class ListViewModel : ViewModel(), SelectedAlgorithmViewModel {
    override val selectedAlgorithmType = SingleLiveEvent<EncryptionAlgorithmType>()

    init {
        Log.w("lol","created: $this")
    }
}

class AlgorithmListFragment : Fragment(), AlgorithmClickListener {

    companion object {
        private val TAG = AlgorithmListFragment::class.java.simpleName

        const val SELECTED_ALGORITHM_KEY = "SELECTED_ALGORITHM_KEY"

        fun newInstance(): AlgorithmListFragment {
            return AlgorithmListFragment()
        }
    }

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private val viewModel: AlgorithmListViewModel by viewModels { viewModelFactory }

    private var algorithmListAdapter = EncryptionAlgorithmListAdapter(this)

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        viewModel.algorithmsList.observe(viewLifecycleOwner, Observer { ciphers ->
            algorithmListAdapter.setAlgorithmList(ciphers)
        })

        viewModel.selectedAlgorithm.observe(viewLifecycleOwner, Observer { algorithmType ->
            findNavController().previousBackStackEntry?.let {
                val resultViewModel = ViewModelProvider(it).get(ListViewModel::class.java)
                resultViewModel.onAlgorithmSelected(algorithmType)
            }
        })
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val activity = activity

        val myApplication = activity?.application as MyApplication

        myApplication.appComponent.inject(this)


        val binding: FragmentAlgorithmsBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_algorithms,
                container, false)

        val view = binding.root

        binding.viewModel = viewModel

        binding.cipherListView.layoutManager = LinearLayoutManager(activity)
        binding.cipherListView.adapter = algorithmListAdapter

        return view
    }

    override fun onClick(algorithmModel: AlgorithmModel) {
        viewModel.onAlgorithmChosen(algorithmModel.algorithmType)
    }
}
