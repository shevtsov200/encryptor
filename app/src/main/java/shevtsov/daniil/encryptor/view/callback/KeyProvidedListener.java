package shevtsov.daniil.encryptor.view.callback;

public interface KeyProvidedListener {
    void onKeyProvided(String decryptionKey);
}
