package shevtsov.daniil.encryptor.view.ui.dialog

import android.annotation.SuppressLint
import android.app.Dialog
import android.os.Bundle
import android.view.LayoutInflater
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import shevtsov.daniil.encryptor.R
import shevtsov.daniil.encryptor.android.MyApplication
import shevtsov.daniil.encryptor.databinding.DialogDecryptionKeyBinding
import shevtsov.daniil.encryptor.util.navigation.KekNavigation
import shevtsov.daniil.encryptor.viewmodel.KeyDialogViewModel
import javax.inject.Inject

class DecryptionKeyDialog : DialogFragment() {
    @Inject
    lateinit var decryptionKeyViewModelFactory: ViewModelProvider.Factory
    private val keyDialogViewModel: KeyDialogViewModel by viewModels { decryptionKeyViewModelFactory }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val myApplication = activity?.application as MyApplication

        myApplication.appComponent.inject(this)

        val binding = DataBindingUtil.inflate<DialogDecryptionKeyBinding>(LayoutInflater.from(getActivity()),
                R.layout.dialog_decryption_key, null, false)
        binding.lifecycleOwner = this
        binding.viewModel = keyDialogViewModel
        binding.executePendingBindings()

        return MaterialAlertDialogBuilder(activity)
                .setView(binding.root)
                .create()
    }

    @SuppressLint("FragmentLiveDataObserve")
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        keyDialogViewModel.provideKeyEvent.observe(this, Observer { decryptionKey ->
            finishWithResult(decryptionKey)
        })
    }

    private fun finishWithResult(key: String) {
        KekNavigation.finishWithResult(
                dialog = this,
                result = key
        )

        dismiss()
    }
}