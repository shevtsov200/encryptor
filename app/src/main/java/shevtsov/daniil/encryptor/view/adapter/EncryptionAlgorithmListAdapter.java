package shevtsov.daniil.encryptor.view.adapter;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;
import java.util.Objects;

import shevtsov.daniil.encryptor.R;
import shevtsov.daniil.encryptor.databinding.EncryptionAlgorithmListItemBinding;
import shevtsov.daniil.encryptor.service.model.AlgorithmModel;
import shevtsov.daniil.encryptor.view.callback.AlgorithmClickListener;

public class EncryptionAlgorithmListAdapter extends RecyclerView.Adapter<EncryptionAlgorithmListAdapter.AlgorithmViewHolder> {
    @Nullable
    private final AlgorithmClickListener mAlgorithmClickListener;
    private List<? extends AlgorithmModel> mAlgorithmList;

    public EncryptionAlgorithmListAdapter(@Nullable AlgorithmClickListener algorithmListener) {
        mAlgorithmClickListener = algorithmListener;
    }

    public void setAlgorithmList(final List<? extends AlgorithmModel> algorithmList) {
        if (mAlgorithmList == null) {
            mAlgorithmList = algorithmList;
            notifyItemRangeInserted(0, algorithmList.size());
        } else {
            DiffUtil.DiffResult result = DiffUtil.calculateDiff(new DiffUtil.Callback() {
                @Override
                public int getOldListSize() {
                    return mAlgorithmList.size();
                }

                @Override
                public int getNewListSize() {
                    return algorithmList.size();
                }

                @Override
                public boolean areItemsTheSame(int oldItemPosition, int newItemPosition) {
                    return mAlgorithmList.get(oldItemPosition).getEncryptionAlgorithmName()
                            .equals(algorithmList.get(newItemPosition).getEncryptionAlgorithmName());
                }

                @Override
                public boolean areContentsTheSame(int oldItemPosition, int newItemPosition) {
                    AlgorithmModel newAlgorithmModel = algorithmList.get(newItemPosition);
                    AlgorithmModel oldAlgorithmModel = algorithmList.get(oldItemPosition);

                    return newAlgorithmModel.getEncryptionAlgorithmName().equals(oldAlgorithmModel.getEncryptionAlgorithmName())
                            && Objects.equals(newAlgorithmModel.getEncryptionAlgorithmName(),
                            oldAlgorithmModel.getEncryptionAlgorithmName());
                }
            });

            mAlgorithmList = algorithmList;

            result.dispatchUpdatesTo(this);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull AlgorithmViewHolder holder, int position) {
        holder.mBinding.setAlgorithmModel(mAlgorithmList.get(position));
        holder.mBinding.executePendingBindings();
    }

    @NonNull
    @Override
    public AlgorithmViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        EncryptionAlgorithmListItemBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()),
                R.layout.encryption_algorithm_list_item, parent, false);

        binding.setCallback(mAlgorithmClickListener);

        return new AlgorithmViewHolder(binding);
    }

    @Override
    public int getItemCount() {
        return mAlgorithmList == null ? 0 : mAlgorithmList.size();
    }

    static class AlgorithmViewHolder extends RecyclerView.ViewHolder {
        final EncryptionAlgorithmListItemBinding mBinding;

        AlgorithmViewHolder(EncryptionAlgorithmListItemBinding binding) {
            super(binding.getRoot());
            mBinding = binding;
        }

    }
}
