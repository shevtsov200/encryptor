package shevtsov.daniil.encryptor.service.model.encryptor;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.security.NoSuchAlgorithmException;

import shevtsov.daniil.encryptor.util.ByteHexConverter;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.core.IsNull.notNullValue;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doReturn;

public class SecretKeyGeneratorTest {
    private static final String VALID_ALGORITHM_NAME = "DES";
    private static final String INVALID_ALGORITHM_NAME = "ZZ";

    private static final String HEX_KEY = "FFF";

    private SecretKeyGenerator mSecretKeyGenerator;

    @Mock
    private ByteHexConverter mByteHexConverter;

    @Before
    public void onStartUp() {
        MockitoAnnotations.initMocks(this);

        doReturn(HEX_KEY).when(mByteHexConverter).bytesToHex(any(byte[].class));

        mSecretKeyGenerator = new SecretKeyGenerator(mByteHexConverter);
    }

    @Test
    public void testGenerateKeyValidAlgorithm() throws NoSuchAlgorithmException {
        mSecretKeyGenerator.generateKey(VALID_ALGORITHM_NAME);

        assertThat(mSecretKeyGenerator.getSecretKey(), is(notNullValue()));
    }

    @Test(expected = NoSuchAlgorithmException.class)
    public void testGenerateKeyInvalidAlgorithm() throws NoSuchAlgorithmException {
        mSecretKeyGenerator.generateKey(INVALID_ALGORITHM_NAME);
    }

    @Test
    public void testEncodedKey() throws NoSuchAlgorithmException {
        mSecretKeyGenerator.generateKey(VALID_ALGORITHM_NAME);

        String encodedKey = mSecretKeyGenerator.getSecretKeyEncoded();
        assertThat(encodedKey, is(HEX_KEY));
    }
}
