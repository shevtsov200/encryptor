package shevtsov.daniil.encryptor.service.model.encryptor;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.io.UnsupportedEncodingException;
import java.security.GeneralSecurityException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import javax.crypto.SecretKey;

import shevtsov.daniil.encryptor.service.repository.EncryptionAlgorithmType;
import shevtsov.daniil.encryptor.util.Logger;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doReturn;

public class EncryptorTest {
    private static final String PLAIN_TEXT = "lol";
    private static final String VALID_CIPHERED_TEXT = "FF";
    private static final String VALID_KEY = "5A";

    private static final String ALGORITHM_WITH_SETTINGS = "FGFGFGDG";

    private static final EncryptionAlgorithmType ALGORITHM_TYPE = EncryptionAlgorithmType.DES;

    private Encryptor mEncryptor;

    @Mock
    Logger mLogger;

    @Mock
    CipherProvider mCipherProvider;

    @Mock
    SecretKey mSecretKey;

    @Mock
    SecretKeyGenerator mSecretKeyGenerator;

    @Mock
    EncryptionAlgorithmProvider mEncryptionAlgorithmProvider;

    @Before
    public void init() throws GeneralSecurityException, UnsupportedEncodingException {
        MockitoAnnotations.initMocks(this);

        doReturn(mSecretKey).when(mSecretKeyGenerator).getSecretKey();

        doReturn(VALID_CIPHERED_TEXT).when(mCipherProvider).encrypt(eq(PLAIN_TEXT), anyString(), any(SecretKey.class));
        doReturn(PLAIN_TEXT).when(mCipherProvider).decrypt(eq(VALID_CIPHERED_TEXT), anyString(), anyString(), anyString());

        doReturn(VALID_KEY).when(mSecretKeyGenerator).getSecretKeyEncoded();

        doReturn(ALGORITHM_WITH_SETTINGS).when(mEncryptionAlgorithmProvider).getAlgorithmWithSettings(ALGORITHM_TYPE);

        mEncryptor = new Encryptor(mLogger, mCipherProvider, mSecretKeyGenerator, mEncryptionAlgorithmProvider);
    }

    @Test
    public void testEncryption() throws GeneralSecurityException, UnsupportedEncodingException {
        mEncryptor.setAlgorithm(ALGORITHM_TYPE);

        String encryptedText = mEncryptor.encryptText(PLAIN_TEXT);

        assertThat(encryptedText, is(VALID_CIPHERED_TEXT));
    }



    @Test
    public void testValidDecryption() throws NoSuchAlgorithmException, InvalidKeyException {
        mEncryptor.setAlgorithm(ALGORITHM_TYPE);

        String decryptedText = mEncryptor.decipherText(VALID_CIPHERED_TEXT, VALID_KEY);

        assertEquals(PLAIN_TEXT, decryptedText);
    }

    @Test
    public void testGetEncodedKey() {
        mEncryptor.setAlgorithm(ALGORITHM_TYPE);

        String encodedKey = mEncryptor.getKey();

        assertThat(encodedKey, is(VALID_KEY));
    }
}
