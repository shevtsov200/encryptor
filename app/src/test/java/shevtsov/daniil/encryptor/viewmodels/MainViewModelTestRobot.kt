package shevtsov.daniil.encryptor.viewmodels

import android.net.Uri
import androidx.lifecycle.Observer
import io.mockk.Called
import io.mockk.every
import io.mockk.mockk
import io.mockk.mockkClass
import io.mockk.slot
import io.mockk.verify
import junit.framework.Assert.assertEquals
import shevtsov.daniil.encryptor.domain.main.LoadSourceTextUseCase
import shevtsov.daniil.encryptor.domain.main.SaveSourceTextUseCase
import shevtsov.daniil.encryptor.service.repository.localstorage.LocalStorageRepository
import shevtsov.daniil.encryptor.ui.main.MainViewModel
import shevtsov.daniil.encryptor.ui.main.MainViewState
import shevtsov.daniil.encryptor.util.Logger
import shevtsov.daniil.encryptor.util.validation.FileExtensionValidator
import shevtsov.daniil.encryptor.util.validation.MaxTextLengthProvider
import shevtsov.daniil.encryptor.util.validation.SourceTextValidator

class MainViewModelTestRobot {

    private lateinit var mainViewModel: MainViewModel

    private val logger: Logger = mockk()
    private val localStorageRepository: LocalStorageRepository = mockk()
    private val sourceTextValidator: SourceTextValidator = mockk()
    private val maxTextLengthProvider: MaxTextLengthProvider = mockk()
    private val fileExtensionValidator: FileExtensionValidator = mockk()

    private val saveSourceTextUseCase: SaveSourceTextUseCase = mockk(relaxUnitFun = true)
    private val loadSourceTextUseCase: LoadSourceTextUseCase = mockk()

    private val mainViewStateObserver: Observer<MainViewState> = mockk(relaxUnitFun = true)

    private val permissionsRequestObserver: Observer<Void> = mockk(relaxUnitFun = true)
    private val fileSelectionObserver: Observer<Void> = mockk(relaxUnitFun = true)
    private val invalidFileExtensionObserver: Observer<Void> = mockk(relaxUnitFun = true)
    private val encryptionEventObserver: Observer<String> = mockk(relaxUnitFun = true)
    private val decryptionEventObserver: Observer<String> = mockk(relaxUnitFun = true)

    //TODO: Get rid of this android dependency in view model
    private val fileUri = mockkClass(Uri::class)

    fun buildViewModel(
            maxTextLength: Int,
            savedSourceText: String,
            savedSourceTextValidity: Boolean = false
    ): MainViewModelTestRobot {
        every { maxTextLengthProvider.getAllowedMaxLength() } returns maxTextLength
        every { loadSourceTextUseCase.invoke() } returns savedSourceText
        every { sourceTextValidator.validateTextForEncryption(savedSourceText, any()) } returns savedSourceTextValidity
        every { sourceTextValidator.validateTextForDecryption(savedSourceText, any()) } returns savedSourceTextValidity


        mainViewModel = MainViewModel(
                logger = logger,
                storeRepository = localStorageRepository,
                sourceTextValidator = sourceTextValidator,
                maxTextLengthProvider = maxTextLengthProvider,
                fileExtensionValidator = fileExtensionValidator,
                saveSourceTextUseCase = saveSourceTextUseCase,
                loadSourceTextUseCase = loadSourceTextUseCase
        )

        mainViewModel.mainViewState.observeForever(mainViewStateObserver)

        mainViewModel.requestPermissionEvent.observeForever(permissionsRequestObserver)
        mainViewModel.chooseFileEvent.observeForever(fileSelectionObserver)
        mainViewModel.invalidFileExtensionEvent.observeForever(invalidFileExtensionObserver)
        mainViewModel.navigateToEncryptionEvent.observeForever(encryptionEventObserver)
        mainViewModel.navigateToDecryptionEvent.observeForever(decryptionEventObserver)

        return this
    }

    fun provideSourceText(
            sourceText: String,
            isValidForEncryption: Boolean = true,
            isValidForDecryption: Boolean = true
    ): MainViewModelTestRobot {
        every { sourceTextValidator.validateTextForEncryption(sourceText, any()) } returns isValidForEncryption
        every { sourceTextValidator.validateTextForDecryption(sourceText, any()) } returns isValidForDecryption

        mainViewModel.onInputChange(sourceText)

        return this
    }

    fun onPermissionStatus(isPermissionGranted: Boolean): MainViewModelTestRobot {
        mainViewModel.onPermissionStatusChanged(isPermissionGranted)

        return this
    }

    fun openFileSelection(): MainViewModelTestRobot {
        mainViewModel.onChooseFile()

        return this
    }

    fun onFileSelected(
            textToLoad: String,
            isFileValid: Boolean = true,
            isTextValidForEncryption: Boolean = true,
            isTextValidForDecryption: Boolean = true): MainViewModelTestRobot {
        //TODO: Clearly too many responsibilities
        every { fileExtensionValidator.validate(fileUri, any()) } returns isFileValid
        every { sourceTextValidator.validateTextForEncryption(textToLoad, any()) } returns isTextValidForEncryption
        every { sourceTextValidator.validateTextForDecryption(textToLoad, any()) } returns isTextValidForDecryption
        every { localStorageRepository.loadText(fileUri) } returns textToLoad

        mainViewModel.onFileChosen(fileUri)

        return this
    }

    fun navigateToEncryption(): MainViewModelTestRobot {
        mainViewModel.onEncryptionChosen()

        return this
    }

    fun navigateToDecryption(): MainViewModelTestRobot {
        mainViewModel.onDecryptionChosen()

        return this
    }

    fun verifyTextEncryptionValidity(expectedValidity: Boolean): MainViewModelTestRobot {
        verifyViewState(expectedValidForEncryption = expectedValidity)

        return this
    }

    fun verifyTextDecryptionValidity(expectedValidity: Boolean): MainViewModelTestRobot {
        verifyViewState(expectedValidForDecryption = expectedValidity)

        return this
    }

    fun verifyTextProvided(expectedText: String): MainViewModelTestRobot {
        verifyViewState(expectedSourceText = expectedText)

        return this
    }

    fun verifyTextCount(expectedCount: Int): MainViewModelTestRobot {
        verifyViewState(expectedCharacterCount = expectedCount)

        return this
    }

    fun finishTyping(): MainViewModelTestRobot {
        mainViewModel.onFinishedTyping()

        return this
    }

    fun verifyTextSaved(expectedSavedText: String): MainViewModelTestRobot {
        verify { saveSourceTextUseCase.invoke(sourceText = expectedSavedText) }

        return this
    }

    fun verifyPermissionRequested(): MainViewModelTestRobot {
        verify { permissionsRequestObserver.onChanged(any()) }

        return this
    }

    fun verifyPermissionNotRequested(): MainViewModelTestRobot {
        verify { permissionsRequestObserver wasNot Called }

        return this
    }

    fun verifyFileSelectionOpened(): MainViewModelTestRobot {
        verify { fileSelectionObserver.onChanged(any()) }

        return this
    }

    fun verifyInvalidFileEvent(): MainViewModelTestRobot {
        verify { invalidFileExtensionObserver.onChanged(any()) }

        return this
    }

    fun verifyPassedTextToEncryption(expectedPassedText: String): MainViewModelTestRobot {
        verify { encryptionEventObserver.onChanged(expectedPassedText) }

        return this
    }

    fun verifyPassedTextToDecryption(expectedPassedText: String): MainViewModelTestRobot {
        verify { decryptionEventObserver.onChanged(expectedPassedText) }

        return this
    }

    private fun verifyViewState(
            expectedSourceText: String? = null,
            expectedCharacterCount: Int? = null,
            expectedMaxCharacters: Int? = null,
            expectedValidForEncryption: Boolean? = null,
            expectedValidForDecryption: Boolean? = null

    ) {
        val viewStateSlot = slot<MainViewState>()

        verify { mainViewStateObserver.onChanged(capture(viewStateSlot)) }

        with(viewStateSlot.captured) {
            expectedSourceText.assertIfNotNull(actual = sourceText)
            expectedCharacterCount.assertIfNotNull(actual = characterCount)
            expectedMaxCharacters.assertIfNotNull(actual = maxCharacters)
            expectedValidForEncryption.assertIfNotNull(actual = isValidForEncryption)
            expectedValidForDecryption.assertIfNotNull(actual = isValidForDecryption)
        }

    }

    private fun <T> T?.assertIfNotNull(actual: T) {
        if (this != null) {
            assertEquals(this, actual)
        }
    }

}
