package shevtsov.daniil.encryptor.viewmodels

import io.mockk.clearAllMocks
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.api.extension.ExtendWith
import shevtsov.daniil.encryptor.util.InstantTaskExecutorExtension

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@ExtendWith(InstantTaskExecutorExtension::class)
class MainViewModelTest {

    companion object {
        private const val SOURCE_TEXT = "aaa"
        private const val SAVED_SOURCE_TEXT = "saved source text"

        private const val MAX_TEXT_LENGTH = 10
    }

    @BeforeEach
    fun init() {
        clearAllMocks()
    }

    @Test
    fun `when text typed - then it is provided`() {
        buildMainViewModelTestRobot()
                .provideSourceText(sourceText = SOURCE_TEXT)
                .verifyTextProvided(expectedText = SOURCE_TEXT)
    }

    @Test
    fun `when text typed - then count updated`() {
        buildMainViewModelTestRobot()
                .provideSourceText(SOURCE_TEXT)
                //TODO: Depends on implementation too much
                .verifyTextCount(expectedCount = SOURCE_TEXT.length)
    }

    @Test
    fun `when finished typing - text is saved`() {
        buildMainViewModelTestRobot()
                .provideSourceText(SOURCE_TEXT)
                .finishTyping()
                .verifyTextSaved(expectedSavedText = SOURCE_TEXT)
    }

    @Test
    fun `when started - text is loaded`() {
        buildMainViewModelTestRobot()
                .verifyTextProvided(expectedText = SAVED_SOURCE_TEXT)
    }

    @Test
    fun `when text is invalid for encryption - validity correctly updated`() {
        buildMainViewModelTestRobot()
                .provideSourceText(sourceText = SOURCE_TEXT, isValidForEncryption = false)
                .verifyTextEncryptionValidity(expectedValidity = false)
    }

    @Test
    fun `when text is valid for encryption - validity correctly updated`() {
        buildMainViewModelTestRobot()
                .provideSourceText(sourceText = SOURCE_TEXT, isValidForEncryption = true)
                .verifyTextEncryptionValidity(expectedValidity = true)
    }

    @Test
    fun `when changing text from valid to invalid - validity changes correctly`() {
        buildMainViewModelTestRobot()
                .provideSourceText(sourceText = SOURCE_TEXT, isValidForEncryption = true)
                .verifyTextEncryptionValidity(expectedValidity = true)
                .provideSourceText(sourceText = SOURCE_TEXT, isValidForEncryption = false)
                .verifyTextEncryptionValidity(expectedValidity = false)
    }

    @Test
    fun `when changing text from invalid to valid - validity changes correctly`() {
        buildMainViewModelTestRobot()
                .provideSourceText(sourceText = SOURCE_TEXT, isValidForEncryption = false)
                .verifyTextEncryptionValidity(expectedValidity = false)
                .provideSourceText(sourceText = SOURCE_TEXT, isValidForEncryption = true)
                .verifyTextEncryptionValidity(expectedValidity = true)
    }

    @Test
    fun `when text is invalid for decryption - validity correctly updated`() {
        buildMainViewModelTestRobot()
                .provideSourceText(sourceText = SOURCE_TEXT, isValidForDecryption = false)
                .verifyTextDecryptionValidity(expectedValidity = false)
    }

    @Test
    fun `when text is valid for decryption - validity correctly updated`() {
        buildMainViewModelTestRobot()
                .provideSourceText(sourceText = SOURCE_TEXT, isValidForDecryption = true)
                .verifyTextDecryptionValidity(expectedValidity = true)
    }

    @Test
    fun `when opening file selection without permissions - permission requested`() {
        buildMainViewModelTestRobot()
                .onPermissionStatus(isPermissionGranted = false)
                .openFileSelection()
                .verifyPermissionRequested()
    }

    @Test
    fun `when opening file selection with permissions - permission not requested`() {
        buildMainViewModelTestRobot()
                .onPermissionStatus(isPermissionGranted = true)
                .openFileSelection()
                .verifyPermissionNotRequested()
    }

    @Test
    fun `when opening file selection with permissions - open file selection`() {
        buildMainViewModelTestRobot()
                .onPermissionStatus(isPermissionGranted = true)
                .openFileSelection()
                .verifyFileSelectionOpened()
    }

    @Test
    fun `when selected file extension invalid - invalid event`() {
        buildMainViewModelTestRobot()
                .onFileSelected(textToLoad = SOURCE_TEXT, isFileValid = false)
                .verifyInvalidFileEvent()
    }

    @Test
    fun `when selected file extension valid - load text from file`() {
        buildMainViewModelTestRobot()
                .onFileSelected(textToLoad = SOURCE_TEXT)
                .verifyTextProvided(expectedText = SOURCE_TEXT)
    }

    @Test
    fun `when text is valid - navigate to encryption`() {
        buildMainViewModelTestRobot()
                .provideSourceText(SOURCE_TEXT)
                .navigateToEncryption()
                .verifyPassedTextToEncryption(expectedPassedText = SOURCE_TEXT)
    }

    @Test
    fun `when text is valid - navigate to decryption`() {
        buildMainViewModelTestRobot()
                .provideSourceText(SOURCE_TEXT)
                .navigateToDecryption()
                .verifyPassedTextToDecryption(expectedPassedText = SOURCE_TEXT)
    }

    private fun buildMainViewModelTestRobot(): MainViewModelTestRobot = MainViewModelTestRobot()
            .buildViewModel(
                    maxTextLength = MAX_TEXT_LENGTH,
                    savedSourceText = SAVED_SOURCE_TEXT
            )
}
