package shevtsov.daniil.encryptor.viewmodels

import androidx.lifecycle.Observer
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import shevtsov.daniil.encryptor.util.Logger
import shevtsov.daniil.encryptor.util.validation.FileNameValidator
import shevtsov.daniil.encryptor.viewmodel.ChooseFileNameViewModel

//TODO: Everything in this view model is named veri similair, need better naming
class FileNameViewModelTestRobot {
    private lateinit var fileNameViewModel: ChooseFileNameViewModel

    private val logger: Logger = mockk()
    private val fileNameValidator: FileNameValidator = mockk()

    private val isFileNameValidObserver: Observer<Boolean> = mockk(relaxUnitFun = true)
    private val fileNameObserver: Observer<String> = mockk(relaxUnitFun = true)
    private val fileNameEventObserver: Observer<String> = mockk(relaxUnitFun = true)

    fun buildViewModel(): FileNameViewModelTestRobot {
        fileNameViewModel = ChooseFileNameViewModel(
                logger = logger,
                fileNameValidator = fileNameValidator
        )

        fileNameViewModel.isFileNameValid.observeForever(isFileNameValidObserver)
        fileNameViewModel.provideFileNameEvent.observeForever(fileNameEventObserver)

        return this
    }

    fun provideFileName(fileName: String): FileNameViewModelTestRobot {
        fileNameViewModel.onInputChange(newFileName = fileName)

        return this
    }

    fun returnFileNameFromDialog(): FileNameViewModelTestRobot {
        fileNameViewModel.returnFileNameFromDialog()

        return this
    }

    fun defineValidity(fileName: String, isValid: Boolean): FileNameViewModelTestRobot {
        every { fileNameValidator.validateFileName(fileName) } answers { isValid }

        return this
    }

    fun verifyIsFileNameValid(expectedValidity: Boolean): FileNameViewModelTestRobot {
        verify { isFileNameValidObserver.onChanged(expectedValidity) }

        return this
    }

    fun verifyFileNameEvent(expectedFileName: String): FileNameViewModelTestRobot {
        verify { fileNameEventObserver.onChanged(expectedFileName) }

        return this
    }


}