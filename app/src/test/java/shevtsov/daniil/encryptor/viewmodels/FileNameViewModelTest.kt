package shevtsov.daniil.encryptor.viewmodels

import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.api.extension.ExtendWith
import shevtsov.daniil.encryptor.util.InstantTaskExecutorExtension

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@ExtendWith(InstantTaskExecutorExtension::class)
class FileNameViewModelTest {
    companion object {
        private const val VALID_FILE_NAME = "aaa"
        private const val INVALID_FILE_NAME = "bbb"
    }

    //TODO: Improve naming of view model file name valid property and this test
    @Test
    fun `when text is valid - file name is valid`() {
        createRobot()
                .provideFileName(fileName = VALID_FILE_NAME)
                .verifyIsFileNameValid(expectedValidity = true)
    }

    @Test
    fun `when text is invalid - file name is invalid`() {
        createRobot()
                .provideFileName(fileName = INVALID_FILE_NAME)
                .verifyIsFileNameValid(expectedValidity = false)
    }

    @Test
    fun `when validity changes - file name validity is updated`() {
        createRobot()
                .provideFileName(fileName = VALID_FILE_NAME)
                .verifyIsFileNameValid(expectedValidity = true)
                .provideFileName(fileName = INVALID_FILE_NAME)
                .verifyIsFileNameValid(expectedValidity = false)
    }

    @Test
    fun `when return file name from dialog - send event with it`() {
        createRobot()
                .provideFileName(fileName = VALID_FILE_NAME)
                .returnFileNameFromDialog()
                .verifyFileNameEvent(expectedFileName = VALID_FILE_NAME)
    }

    private fun createRobot() = FileNameViewModelTestRobot()
            .buildViewModel()
            .defineValidity(fileName = VALID_FILE_NAME, isValid = true)
            .defineValidity(fileName = INVALID_FILE_NAME, isValid = false)
}