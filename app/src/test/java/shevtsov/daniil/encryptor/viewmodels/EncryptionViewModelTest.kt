package shevtsov.daniil.encryptor.viewmodels

import io.mockk.clearAllMocks
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.api.extension.ExtendWith
import shevtsov.daniil.encryptor.service.repository.EncryptionAlgorithmType
import shevtsov.daniil.encryptor.util.InstantTaskExecutorExtension

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@ExtendWith(InstantTaskExecutorExtension::class)
class EncryptionViewModelTest {
    companion object {
        private const val PLAIN_TEXT = "aaaabbbb"
        private const val ENCRYPTED_TEXT = "cccccddddd"
        private const val ENCRYPTION_KEY = "zzzz"

        private val SELECTED_ALGORITHM = EncryptionAlgorithmType.DES
    }

    @BeforeEach
    fun init() {
        clearAllMocks()
    }

    @Test
    fun `when algorithm chosen - result contains encrypted text`() {
        EncryptionViewModelTestRobot()
                .buildViewModel()
                //TODO: Too much implementation details
                .defineEncryption(plainText = PLAIN_TEXT, encryptedText = ENCRYPTED_TEXT)
                .defineEncryptionKey(encryptionKey = ENCRYPTION_KEY)
                .provideTextForEncryption(textForEncryption = PLAIN_TEXT)
                .selectAlgorithm(selectedAlgorithmType = SELECTED_ALGORITHM)
                .validateResultContainsText(expectedText = ENCRYPTED_TEXT)

    }
}