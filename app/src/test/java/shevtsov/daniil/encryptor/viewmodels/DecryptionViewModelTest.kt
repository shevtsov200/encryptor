package shevtsov.daniil.encryptor.viewmodels

import io.mockk.clearAllMocks
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.api.extension.ExtendWith
import shevtsov.daniil.encryptor.service.repository.EncryptionAlgorithmType
import shevtsov.daniil.encryptor.util.InstantTaskExecutorExtension

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@ExtendWith(InstantTaskExecutorExtension::class)
class DecryptionViewModelTest {

    companion object {
        private const val DECRYPTION_KEY = "aaa"
        private const val ENCRYPED_TEXT = "bbb"
        private const val DECRYPTED_TEXT = "ccc"

        private val SELECTED_ALGORITHM = EncryptionAlgorithmType.DES
    }

    @BeforeEach
    fun init() {
        clearAllMocks()
    }

    @Test
    fun `when algorithm selected - request key`() {
        DecryptionViewModelTestRobot()
                .buildViewModel()
                .selectAlgorithm(selectedAlgorithm = SELECTED_ALGORITHM)
                .validateDecryptionKeyRequested()
    }

    @Test
    fun `when algorithm selected - decrypted text in result`() {
        DecryptionViewModelTestRobot()
                .buildViewModel()
                .defineDecryption(encryptedText = ENCRYPED_TEXT, decryptedText = DECRYPTED_TEXT)
                .provideText(textForDecryption = ENCRYPED_TEXT)
                .selectAlgorithm(selectedAlgorithm = SELECTED_ALGORITHM)
                .provideDecryptionKey(decryptionKey = DECRYPTION_KEY)
                .validateResultContainsDecryptedText(expectedText = DECRYPTED_TEXT)
    }
}