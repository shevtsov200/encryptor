package shevtsov.daniil.encryptor.viewmodels

import androidx.lifecycle.Observer
import io.mockk.mockk
import io.mockk.verify
import shevtsov.daniil.encryptor.service.repository.localstorage.LocalStorageRepository
import shevtsov.daniil.encryptor.util.Logger
import shevtsov.daniil.encryptor.util.android.ClipboardRepository
import shevtsov.daniil.encryptor.viewmodel.ResultViewModel

class ResultViewModelTestRobot {
    private lateinit var resultViewmodel: ResultViewModel

    private val logger: Logger = mockk()
    private val localStorageRepository: LocalStorageRepository = mockk(relaxUnitFun = true)
    private val clipboardRepository: ClipboardRepository = mockk(relaxUnitFun = true)

    private val resultTextObserver: Observer<String> = mockk(relaxUnitFun = true)
    private val hasKeyObserver: Observer<Boolean> = mockk(relaxUnitFun = true)
    private val encryptionKeyObserver: Observer<String> = mockk(relaxUnitFun = true)

    private val shareTextEventObserver: Observer<String> = mockk(relaxUnitFun = true)
    private val showChooseFileEventObserver: Observer<Void> = mockk(relaxUnitFun = true)

    fun buildViewModel(): ResultViewModelTestRobot {
        resultViewmodel = ResultViewModel(
                logger = logger,
                localStorageRepository = localStorageRepository,
                clipboardRepsitory = clipboardRepository
        )

        resultViewmodel.resultText.observeForever(resultTextObserver)
        resultViewmodel.hasKey.observeForever(hasKeyObserver)
        resultViewmodel.encryptionKey.observeForever(encryptionKeyObserver)

        resultViewmodel.shareTextEvent.observeForever(shareTextEventObserver)
        resultViewmodel.chooseFileEvent.observeForever(showChooseFileEventObserver)

        return this
    }

    fun provideText(text: String): ResultViewModelTestRobot {
        resultViewmodel.setResultText(resultText = text)

        return this
    }

    fun provideEncryptionKey(key: String): ResultViewModelTestRobot {
        resultViewmodel.setEncryptionKey(encryptionKey = key)

        return this
    }

    fun copyText(): ResultViewModelTestRobot {
        resultViewmodel.onCopyText()

        return this
    }

    fun copyEncryptionKey(): ResultViewModelTestRobot {
        resultViewmodel.onCopyKey()

        return this
    }

    fun shareText(): ResultViewModelTestRobot {
        resultViewmodel.onShareText()

        return this
    }

    fun requestFileNameSelection(): ResultViewModelTestRobot {
        resultViewmodel.onSaveText()

        return this
    }

    fun provideFileName(fileName:String): ResultViewModelTestRobot {
        resultViewmodel.onFileNameProvided(fileName)

        return this
    }

    fun validateEncryptionKeyShown(isShown: Boolean): ResultViewModelTestRobot {
        verify { hasKeyObserver.onChanged(isShown) }

        return this
    }

    fun validateEncryptionKey(expectedKey: String): ResultViewModelTestRobot {
        verify { encryptionKeyObserver.onChanged(expectedKey) }

        return this
    }

    fun validateResultText(expectedText: String): ResultViewModelTestRobot {
        verify { resultTextObserver.onChanged(expectedText) }

        return this
    }

    fun validateTextIsSentToClipboard(sentText: String): ResultViewModelTestRobot {
        verify { clipboardRepository.copyToClipboard(sentText) }

        return this
    }

    fun validateSharedText(sharedText: String): ResultViewModelTestRobot {
        verify { shareTextEventObserver.onChanged(sharedText) }

        return this
    }

    fun validateChooseFileDialogShown(): ResultViewModelTestRobot {
        verify { showChooseFileEventObserver.onChanged(any()) }

        return this
    }

    //TODO: Ideally this should be separated, one method for file name and another - for text
    fun validateTextSavedToFile(
            expectedFileName: String,
            expectedSavedText: String
    ): ResultViewModelTestRobot {
        verify { localStorageRepository.saveText(expectedFileName, expectedSavedText) }

        return this
    }
}