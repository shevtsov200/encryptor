package shevtsov.daniil.encryptor.viewmodels

import io.mockk.clearAllMocks
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.api.extension.ExtendWith
import shevtsov.daniil.encryptor.util.InstantTaskExecutorExtension

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@ExtendWith(InstantTaskExecutorExtension::class)
class ResultViewModelTest {

    companion object {
        private const val RESULT_TEXT = "aaa"
        private const val ENCRYPTION_KEY = "bbb"
        private const val FILE_NAME = "ccc"
    }

    @BeforeEach
    fun init() {
        clearAllMocks()
    }

    @Test
    fun `when text provided - then it is shown`() {
        ResultViewModelTestRobot()
                .buildViewModel()
                .provideText(text = RESULT_TEXT)
                .validateResultText(expectedText = RESULT_TEXT)
    }

    @Test
    fun `when encryption key provided - it is shown`() {
        ResultViewModelTestRobot()
                .buildViewModel()
                .provideEncryptionKey(key = ENCRYPTION_KEY)
                .validateEncryptionKeyShown(isShown = true)
                .validateEncryptionKey(expectedKey = ENCRYPTION_KEY)
    }

    @Test
    fun `when encryption key not provided - it is not shown`() {
        ResultViewModelTestRobot()
                .buildViewModel()
                .validateEncryptionKeyShown(isShown = false)
    }

    @Test
    fun `when text is copied - it is sent to clipboard`() {
        ResultViewModelTestRobot()
                .buildViewModel()
                .provideText(text = RESULT_TEXT)
                .copyText()
                .validateTextIsSentToClipboard(sentText = RESULT_TEXT)
    }

    @Test
    fun `when encryption key is copied - it is sent to clipboard`() {
        ResultViewModelTestRobot()
                .buildViewModel()
                .provideEncryptionKey(key = ENCRYPTION_KEY)
                .copyEncryptionKey()
                .validateTextIsSentToClipboard(sentText = ENCRYPTION_KEY)
    }

    @Test
    fun `when sharing text - it is shared`() {
        ResultViewModelTestRobot()
                .buildViewModel()
                .provideText(text = RESULT_TEXT)
                .shareText()
                .validateSharedText(sharedText = RESULT_TEXT)
    }

    @Test
    fun `when file name selection is requested - request file name`() {
        ResultViewModelTestRobot()
                .buildViewModel()
                .requestFileNameSelection()
                .validateChooseFileDialogShown()
    }

    @Test
    fun `when file name is provided - the text is saved`() {
        ResultViewModelTestRobot()
                .buildViewModel()
                .provideText(text = RESULT_TEXT)
                .provideFileName(fileName = FILE_NAME)
                .validateTextSavedToFile(
                        expectedFileName = FILE_NAME,
                        expectedSavedText = RESULT_TEXT
                )
    }

}