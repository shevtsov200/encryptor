package shevtsov.daniil.encryptor.viewmodels

import androidx.lifecycle.Observer
import io.mockk.every
import io.mockk.mockk
import io.mockk.slot
import io.mockk.verify
import org.junit.Assert.assertEquals
import shevtsov.daniil.encryptor.android.model.CipherResult
import shevtsov.daniil.encryptor.service.model.AlgorithmModel
import shevtsov.daniil.encryptor.service.model.encryptor.Encryptor
import shevtsov.daniil.encryptor.service.repository.EncryptionAlgorithmType
import shevtsov.daniil.encryptor.service.repository.cipher.EncryptionAlgorithmRepository
import shevtsov.daniil.encryptor.util.Logger
import shevtsov.daniil.encryptor.viewmodel.DecryptionViewModel

class DecryptionViewModelTestRobot {
    companion object {
        private val algorithms = listOf(
                AlgorithmModel(EncryptionAlgorithmType.AES, "aes"),
                AlgorithmModel(EncryptionAlgorithmType.DES, "des")
        )
    }

    private lateinit var decryptionViewModel: DecryptionViewModel

    private val logger: Logger = mockk()
    private val encryptor: Encryptor = mockk(relaxUnitFun = true)
    private val encryptionAlgorithmRepository: EncryptionAlgorithmRepository = mockk()

    private val requestKeyEventObserver: Observer<Void> = mockk(relaxUnitFun = true)
    private val decryptionResultObserver: Observer<CipherResult> = mockk(relaxUnitFun = true)

    fun buildViewModel(): DecryptionViewModelTestRobot {
        every { encryptionAlgorithmRepository.algorithmList } answers { algorithms }

        decryptionViewModel = DecryptionViewModel(
                logger = logger,
                encryptor = encryptor,
                encryptionAlgorithmRepository = encryptionAlgorithmRepository
        )


        decryptionViewModel.requestKeyEvent.observeForever(requestKeyEventObserver)
        decryptionViewModel.navigateToResultEvent.observeForever(decryptionResultObserver)

        return this
    }

    fun provideText(textForDecryption: String): DecryptionViewModelTestRobot {
        decryptionViewModel.onEncryptedTextProvided(textForDecryption)

        return this
    }

    fun selectAlgorithm(selectedAlgorithm: EncryptionAlgorithmType): DecryptionViewModelTestRobot {
        decryptionViewModel.onAlgorithmChosen(selectedAlgorithm)

        return this
    }

    fun provideDecryptionKey(decryptionKey: String): DecryptionViewModelTestRobot {
        decryptionViewModel.onKeyProvided(decryptionKey = decryptionKey)

        return this
    }

    fun defineDecryption(
            encryptedText: String,
            decryptedText: String
    ): DecryptionViewModelTestRobot {
        every { encryptor.decipherText(encryptedText, any()) } answers { decryptedText }

        return this
    }

    fun validateDecryptionKeyRequested(): DecryptionViewModelTestRobot {
        verify { requestKeyEventObserver.onChanged(any()) }

        return this
    }

    fun validateResultContainsDecryptedText(expectedText: String): DecryptionViewModelTestRobot {
        val decryptionResultSlot = slot<CipherResult>()

        verify { decryptionResultObserver.onChanged(capture(decryptionResultSlot)) }

        assertEquals(expectedText, decryptionResultSlot.captured.resultText)

        return this
    }
}