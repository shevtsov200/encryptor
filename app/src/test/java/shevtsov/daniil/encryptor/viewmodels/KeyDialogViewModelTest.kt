package shevtsov.daniil.encryptor.viewmodels

import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.api.extension.ExtendWith
import shevtsov.daniil.encryptor.util.InstantTaskExecutorExtension

//TODO: Something is wrong with dialog view models tests or logic, the test is too dependant on inner logic
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@ExtendWith(InstantTaskExecutorExtension::class)
class KeyDialogViewModelTest {

    companion object {
        private const val INVALID_KEY = "zzzz"
        private const val VALID_KEY = "aaa"
    }

    @Test
    fun `when key is invalid - key is invalid`() {
        testKeyValidity(
                key = INVALID_KEY,
                isValid = false
        )
    }

    @Test
    fun `when key is valid - key is valid`() {
        testKeyValidity(
                key = VALID_KEY,
                isValid = true
        )
    }

    @Test
    fun `when valid key provided - event is called`() {
        KeyDialogViewModelTestRobot()
                .buildViewModel()
                .defineKeyValidity(key = VALID_KEY, isValid = true)
                .enterKey(key = VALID_KEY)
                .provideKey()
                .verifyKeyProvided(key = VALID_KEY)
    }

    private fun testKeyValidity(
            key: String,
            isValid: Boolean
    ) {
        KeyDialogViewModelTestRobot()
                .buildViewModel()
                .defineKeyValidity(key = key, isValid = isValid)
                .enterKey(key = key)
                .verifyKeyValidity(isKeyValid = isValid)
    }
}