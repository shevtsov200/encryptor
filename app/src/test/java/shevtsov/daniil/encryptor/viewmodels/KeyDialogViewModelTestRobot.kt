package shevtsov.daniil.encryptor.viewmodels

import androidx.lifecycle.Observer
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import shevtsov.daniil.encryptor.util.Logger
import shevtsov.daniil.encryptor.util.validation.HexTextValidator
import shevtsov.daniil.encryptor.viewmodel.KeyDialogViewModel

class KeyDialogViewModelTestRobot {
    private lateinit var keyDialogViewModel: KeyDialogViewModel

    private val logger: Logger = mockk()
    private val hexTextValidator: HexTextValidator = mockk()

    private val isKeyValidObserver: Observer<Boolean> = mockk(relaxUnitFun = true)
    private val provideKeyEventObserver: Observer<String> = mockk(relaxUnitFun = true)

    fun buildViewModel(): KeyDialogViewModelTestRobot {
        keyDialogViewModel = KeyDialogViewModel(
                logger = logger,
                hexTextValidator = hexTextValidator
        )

        keyDialogViewModel.isKeyValid.observeForever(isKeyValidObserver)
        keyDialogViewModel.provideKeyEvent.observeForever(provideKeyEventObserver)

        return this
    }

    fun enterKey(key: String): KeyDialogViewModelTestRobot {
        keyDialogViewModel.onInputChange(keyText = key)

        return this
    }

    fun provideKey(): KeyDialogViewModelTestRobot {
        keyDialogViewModel.onKeyProvided()

        return this
    }

    fun defineKeyValidity(key: String, isValid: Boolean): KeyDialogViewModelTestRobot {
        every { hexTextValidator.validateTextIsHex(key) } answers { isValid }

        return this
    }

    fun verifyKeyValidity(isKeyValid: Boolean): KeyDialogViewModelTestRobot {
        verify { isKeyValidObserver.onChanged(isKeyValid) }

        return this
    }

    fun verifyKeyProvided(key:String): KeyDialogViewModelTestRobot {
        verify { provideKeyEventObserver.onChanged(key) }
        return this
    }


}