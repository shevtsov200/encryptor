package shevtsov.daniil.encryptor.viewmodels

import androidx.lifecycle.Observer
import io.mockk.every
import io.mockk.mockk
import io.mockk.slot
import io.mockk.verify
import org.junit.jupiter.api.Assertions.assertEquals
import shevtsov.daniil.encryptor.android.model.CipherResult
import shevtsov.daniil.encryptor.service.model.AlgorithmModel
import shevtsov.daniil.encryptor.service.model.encryptor.Encryptor
import shevtsov.daniil.encryptor.service.repository.EncryptionAlgorithmType
import shevtsov.daniil.encryptor.service.repository.cipher.EncryptionAlgorithmRepository
import shevtsov.daniil.encryptor.util.Logger
import shevtsov.daniil.encryptor.viewmodel.EncryptionViewModel

class EncryptionViewModelTestRobot {
    companion object {
        private val algorithms = listOf(
                AlgorithmModel(EncryptionAlgorithmType.AES, "aes"),
                AlgorithmModel(EncryptionAlgorithmType.DES, "des")
        )
    }

    private lateinit var encryptionViewModel: EncryptionViewModel

    private val logger: Logger = mockk()
    private val encryptor: Encryptor = mockk(relaxUnitFun = true)
    private val encryptionAlgorithmRepository: EncryptionAlgorithmRepository = mockk()

    private val encryptionResultObserver: Observer<CipherResult> = mockk(relaxUnitFun = true)

    fun buildViewModel(): EncryptionViewModelTestRobot {
        every { encryptionAlgorithmRepository.algorithmList } answers { algorithms }

        encryptionViewModel = EncryptionViewModel(
                logger = logger,
                encryptor = encryptor,
                encryptionAlgorithmRepository = encryptionAlgorithmRepository
        )

        encryptionViewModel.navigateToResultEvent.observeForever(encryptionResultObserver)

        return this
    }

    fun provideTextForEncryption(textForEncryption: String): EncryptionViewModelTestRobot {
        encryptionViewModel.onPlainTextProvided(plainText = textForEncryption)

        return this
    }

    fun selectAlgorithm(selectedAlgorithmType: EncryptionAlgorithmType): EncryptionViewModelTestRobot {
        encryptionViewModel.onAlgorithmChosen(encryptionAlgorithmType = selectedAlgorithmType)

        return this
    }

    fun validateResultContainsText(expectedText: String): EncryptionViewModelTestRobot {
        val encryptionResultSlot = slot<CipherResult>()

        verify { encryptionResultObserver.onChanged(capture(encryptionResultSlot)) }

        assertEquals(expectedText, encryptionResultSlot.captured.resultText)

        return this
    }

    fun defineEncryption(
            plainText: String,
            encryptedText: String
    ): EncryptionViewModelTestRobot {
        every { encryptor.encryptText(plainText) } answers { encryptedText }

        return this
    }

    fun defineEncryptionKey(
            encryptionKey: String
    ): EncryptionViewModelTestRobot {
        every { encryptor.key } answers { encryptionKey }

        return this
    }

}