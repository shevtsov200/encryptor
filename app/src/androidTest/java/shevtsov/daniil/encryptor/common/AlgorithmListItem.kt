package shevtsov.daniil.encryptor.common

import android.view.View
import com.agoda.kakao.recycler.KRecyclerItem
import com.agoda.kakao.text.KTextView
import org.hamcrest.Matcher
import shevtsov.daniil.encryptor.R

class AlgorithmListItem(parent: Matcher<View>) : KRecyclerItem<AlgorithmListItem>(parent) {
    val title = KTextView(parent) { withId(R.id.algorithm_name_text_view) }
}