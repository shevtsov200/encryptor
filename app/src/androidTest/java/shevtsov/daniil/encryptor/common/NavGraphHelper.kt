package shevtsov.daniil.encryptor.common

import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.fragment.app.testing.launchFragmentInContainer
import androidx.navigation.NavController
import androidx.navigation.Navigation
import shevtsov.daniil.encryptor.R

object NavGraphHelper {
    inline fun <reified NavFragment : Fragment> initTestNavGraph(
            navController: NavController,
            fragmentArguments: Bundle = Bundle.EMPTY
    ) {

        val titleScenario = launchFragmentInContainer<NavFragment>(
                fragmentArgs = fragmentArguments,
                themeResId = R.style.AppTheme
        )

        titleScenario.onFragment { fragment ->
            Navigation.setViewNavController(fragment.requireView(), navController)
        }
    }

    fun assertNavGraphDestination(
            navController: NavController,
            expectedDestinationId: Int
    ) {
        assert(navController.currentDestination?.id == expectedDestinationId)
    }
}