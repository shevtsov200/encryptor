package shevtsov.daniil.encryptor;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.mockito.MockitoAnnotations;

import java.io.UnsupportedEncodingException;
import java.security.GeneralSecurityException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import shevtsov.daniil.encryptor.service.model.encryptor.CipherProvider;
import shevtsov.daniil.encryptor.service.model.encryptor.EncryptionAlgorithmProvider;
import shevtsov.daniil.encryptor.service.model.encryptor.Encryptor;
import shevtsov.daniil.encryptor.service.model.encryptor.SecretKeyGenerator;
import shevtsov.daniil.encryptor.service.repository.EncryptionAlgorithmType;
import shevtsov.daniil.encryptor.util.ByteHexConverter;
import shevtsov.daniil.encryptor.util.Logger;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

@RunWith(Parameterized.class)
public class EncryptorTest {
    private static final String DES_KEY = "49B34357E0404058";
    private static final String DES_SOURCE_TEXT = "abcd";
    private static final String DES_ENCRYPTED_TEXT = "6C2EDB7A96425A96";

    private static final List<EncryptionAlgorithmType> ALGORITHM_TYPES = new ArrayList<>(Arrays.asList(EncryptionAlgorithmType.values()));

    private EncryptionAlgorithmType mEncryptionAlgorithmType;

    @Parameterized.Parameters(name = "{index}: algorithm {0}")
    public static Collection<Object[]> data() {
        Collection<Object[]> parameters = new ArrayList<>();
        for(EncryptionAlgorithmType type : ALGORITHM_TYPES) {
            parameters.add(new Object[]{type});
        }

        return parameters;
    }


    private Encryptor mEncryptor;

    public EncryptorTest(EncryptionAlgorithmType encryptionAlgorithmType) {
        mEncryptionAlgorithmType = encryptionAlgorithmType;
    }

    @Before
    public void onSetup() {
        MockitoAnnotations.initMocks(this);

        ByteHexConverter byteHexConverter = new ByteHexConverter();
        Logger logger = new Logger();
        CipherProvider cipherProvider = new CipherProvider(byteHexConverter, logger);
        EncryptionAlgorithmProvider encryptionAlgorithmProvider = new EncryptionAlgorithmProvider();

        SecretKeyGenerator secretKeyGenerator = new SecretKeyGenerator(byteHexConverter);

        mEncryptor = new Encryptor(logger, cipherProvider, secretKeyGenerator, encryptionAlgorithmProvider);
    }

    @Test
    public void testEncryption() throws GeneralSecurityException, UnsupportedEncodingException {
        mEncryptor.setAlgorithm(mEncryptionAlgorithmType);

        String encryptedText = mEncryptor.encryptText(DES_SOURCE_TEXT);
        String key = mEncryptor.getKey();
        String decryptedText = mEncryptor.decipherText(encryptedText, key);

        assertThat(decryptedText, is(DES_SOURCE_TEXT));
    }
}
