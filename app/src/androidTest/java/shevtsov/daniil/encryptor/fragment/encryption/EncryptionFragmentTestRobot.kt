package shevtsov.daniil.encryptor.fragment.encryption

import android.os.Bundle
import androidx.navigation.testing.TestNavHostController
import androidx.test.core.app.ApplicationProvider
import shevtsov.daniil.encryptor.R
import shevtsov.daniil.encryptor.common.AlgorithmListItem
import shevtsov.daniil.encryptor.common.NavGraphHelper
import shevtsov.daniil.encryptor.view.ui.fragment.EncryptionFragment

class EncryptionFragmentTestRobot(fragmentArguments: Bundle) {
    private val screen = EncryptionFragmentScreen()

    private val navController = TestNavHostController(ApplicationProvider.getApplicationContext()).apply {
        setGraph(R.navigation.main_navigation)
        setCurrentDestination(R.id.encryption_fragment, fragmentArguments)
    }

    init {
        NavGraphHelper.initTestNavGraph<EncryptionFragment>(
                navController = navController,
                fragmentArguments = fragmentArguments
        )
    }

    fun selectAlgorithm(algorithmName: String) {
        screen {
            cipherAlgorithms {
                childWith<AlgorithmListItem> {
                    withDescendant { withText(algorithmName) }
                } perform {
                    click()
                }
            }
        }
    }

    fun verifyNavigateToResult() {
        NavGraphHelper.assertNavGraphDestination(
                navController = navController,
                expectedDestinationId = R.id.result_fragment
        )

        //TODO: verify that correct data was passed to result fragment
    }

}