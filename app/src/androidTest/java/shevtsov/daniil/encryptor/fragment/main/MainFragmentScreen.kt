package shevtsov.daniil.encryptor.fragment.main

import com.agoda.kakao.edit.KEditText
import com.agoda.kakao.screen.Screen
import com.agoda.kakao.text.KButton
import shevtsov.daniil.encryptor.R

class MainFragmentScreen : Screen<MainFragmentScreen>() {
    val sourceTextView = KEditText { withId(R.id.source_text_view) }

    val encryptButton = KButton { withId(R.id.encrypt_button) }
    val decryptButton = KButton { withId(R.id.decrypt_button) }
}
