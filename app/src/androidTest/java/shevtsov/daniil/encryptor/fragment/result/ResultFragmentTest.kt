package shevtsov.daniil.encryptor.fragment.result

import androidx.test.ext.junit.runners.AndroidJUnit4
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class ResultFragmentTest {
    companion object {
        private const val ENCRYPTED_TEXT = "kek kek kek"
        private const val ENCRYPTION_KEY = "FFFF"

        private const val DECRYPTED_TEXT = "ggg"
    }

    @Test
    fun testGotEncryptedText() = withResultTestRobot {
        verifyResultText(expectedText = ENCRYPTED_TEXT)
    }

    @Test
    fun testEncryptionKey() = withResultTestRobot {
        verifyEncryptionKey(expectedKey = ENCRYPTION_KEY)
    }

    @Test
    fun testGotDecryptedText() = withResultTestRobot {
        initForDecryption(decryptedText = DECRYPTED_TEXT)

        verifyResultText(expectedText = DECRYPTED_TEXT)
    }

    @Test
    fun testKeyFieldNotVisibleOnDecryption() = withResultTestRobot {
        initForDecryption(decryptedText = DECRYPTED_TEXT)

        verifyEncryptionKeyShown(isShown = false)
        verifyCopyKeyButtonShown(isShown = false)
    }

    @Test
    fun testCopyTextToClipboard() = withResultTestRobot {
        copyResultText()
        verifyInClipboard(expectedText = ENCRYPTED_TEXT)
    }

    @Test
    fun testShowChooseFileDialog() = withResultTestRobot {
        saveText()
        verifyFileDialogShown(isShown = true)
    }

    @Test
    fun testCopyKeyToClipboard() = withResultTestRobot {
        copyEncryptionKey()

        verifyInClipboard(expectedText = ENCRYPTION_KEY)
    }

    private fun withResultTestRobot(fn: ResultFragmentTestRobot.() -> Unit) = ResultFragmentTestRobot(
            encryptedText = ENCRYPTED_TEXT,
            encryptionKey = ENCRYPTION_KEY
    ).run(fn)
}