package shevtsov.daniil.encryptor.fragment.decryption

import androidx.test.espresso.Espresso.pressBack
import androidx.test.ext.junit.runners.AndroidJUnit4
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import shevtsov.daniil.encryptor.ui.main.MainFragmentDirections

@RunWith(AndroidJUnit4::class)
class DecryptionFragmentTest {

    companion object {
        private const val ALGORITHM_NAME = "DES"

        private const val ENCRYPTED_TEXT = "2F449C80FE675175"
        private const val VALID_KEY = "DA291ADF51D38C58"

        private val fragmentArguments = MainFragmentDirections.navigateToDecryption(sourceText = ENCRYPTED_TEXT).arguments
    }

    @Before
    fun onSetup() {

    }

    @Test
    fun testShowingKeyDialog() = withDecryptionTestRobot {
        selectAlgorithm(algorithmName = ALGORITHM_NAME)

        verifyKeyDialog(isShown = true)
    }

    @Test
    fun testClosingKeyDialog() = withDecryptionTestRobot {
        selectAlgorithm(algorithmName = ALGORITHM_NAME)

        pressBack()

        verifyKeyDialog(isShown = false)
    }

    @Test
    fun testKeyDialogEmptyKey() = withDecryptionTestRobot {
        selectAlgorithm(algorithmName = ALGORITHM_NAME)

        verifyKeyDialogButton(isEnabled = false)
    }

    @Test
    fun testKeyDialogValidKey() = withDecryptionTestRobot {
        selectAlgorithm(algorithmName = ALGORITHM_NAME)
        enterDecryptionKey(key = VALID_KEY)

        verifyKeyDialogButton(isEnabled = true)
    }

    @Test
    fun testNavigateToResult() = withDecryptionTestRobot {
        selectAlgorithm(algorithmName = ALGORITHM_NAME)
        enterDecryptionKey(key = VALID_KEY)

        verifyNavigateToResult()
    }

    private fun withDecryptionTestRobot(fn: DecryptionFragmentTestRobot.() -> Unit) = DecryptionFragmentTestRobot(fragmentArguments).run(fn)

}