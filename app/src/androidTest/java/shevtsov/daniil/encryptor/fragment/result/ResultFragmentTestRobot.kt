package shevtsov.daniil.encryptor.fragment.result

import android.content.ClipboardManager
import android.content.Context
import android.os.Bundle
import androidx.navigation.testing.TestNavHostController
import androidx.test.core.app.ApplicationProvider
import androidx.test.platform.app.InstrumentationRegistry
import junit.framework.Assert
import shevtsov.daniil.encryptor.R
import shevtsov.daniil.encryptor.android.model.CipherResult
import shevtsov.daniil.encryptor.common.NavGraphHelper
import shevtsov.daniil.encryptor.view.ui.fragment.DecryptionFragmentDirections
import shevtsov.daniil.encryptor.view.ui.fragment.EncryptionFragmentDirections
import shevtsov.daniil.encryptor.view.ui.fragment.ResultFragment

class ResultFragmentTestRobot(
        encryptedText: String,
        encryptionKey: String
) {

    private val screen = ResultFragmentScreen()

    private lateinit var navController: TestNavHostController

    init {
        initForEncryption(encryptedText, encryptionKey)
    }

    private fun initForEncryption(
            encryptedText: String,
            encryptionKey: String
    ) {
        val encryptionArguments = EncryptionFragmentDirections.navigateFromEncryptionToResult(cipherResult = CipherResult(
                resultText = encryptedText,
                encryptionKey = encryptionKey
        )).arguments

        initFragment(encryptionArguments)
    }

    fun initForDecryption(decryptedText: String) {
        val decryptionArguments = DecryptionFragmentDirections.navigateFromDecryptionToResult(cipherResult = CipherResult(
                resultText = decryptedText
        )).arguments

        initFragment(decryptionArguments)
    }

    fun copyEncryptionKey() {
        screen {
            copyKeyButton { click() }
        }
    }

    fun copyResultText() {
        screen {
            copyResultButton { click() }
        }
    }

    fun saveText() {
        screen {
            saveTextButton { click() }
        }
    }

    fun verifyResultText(expectedText: String) {
        screen {
            resultTextView {
                hasText(expectedText)
            }
        }
    }

    fun verifyEncryptionKey(expectedKey: String) {
        screen {
            encryptionKeyTextView {
                hasText(expectedKey)
            }
        }
    }

    fun verifyEncryptionKeyShown(isShown: Boolean) {
        screen {
            encryptionKeyTextView {
                if (isShown) {
                    isDisplayed()
                } else {
                    isNotDisplayed()
                }
            }
        }
    }

    fun verifyCopyKeyButtonShown(isShown: Boolean) {
        screen {
            copyKeyButton {
                if (isShown) {
                    isDisplayed()
                } else {
                    isNotDisplayed()
                }
            }
        }
    }

    fun verifyFileDialogShown(isShown: Boolean) {
        screen {
            fileDialogTextView {
                if (isShown) {
                    isDisplayed()
                } else {
                    doesNotExist()
                }
            }
        }
    }

    fun verifyInClipboard(expectedText: String) {
        InstrumentationRegistry.getInstrumentation().runOnMainSync {
            val instrumentationContext = InstrumentationRegistry.getInstrumentation().context
            val clipboardManager: ClipboardManager? = instrumentationContext.getSystemService(Context.CLIPBOARD_SERVICE) as? ClipboardManager
            Assert.assertEquals(clipboardManager?.primaryClip?.getItemAt(0)?.coerceToText(instrumentationContext), expectedText)
        }
    }

    private fun initFragment(arguments: Bundle) {
        navController = TestNavHostController(ApplicationProvider.getApplicationContext()).apply {
            setGraph(R.navigation.main_navigation)
            setCurrentDestination(R.id.result_fragment, arguments)
        }

        NavGraphHelper.initTestNavGraph<ResultFragment>(
                navController = navController,
                fragmentArguments = arguments
        )
    }

}