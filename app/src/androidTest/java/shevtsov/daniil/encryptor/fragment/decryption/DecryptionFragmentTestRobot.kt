package shevtsov.daniil.encryptor.fragment.decryption

import android.os.Bundle
import androidx.navigation.testing.TestNavHostController
import androidx.test.core.app.ApplicationProvider
import shevtsov.daniil.encryptor.R
import shevtsov.daniil.encryptor.common.AlgorithmListItem
import shevtsov.daniil.encryptor.common.NavGraphHelper
import shevtsov.daniil.encryptor.view.ui.fragment.DecryptionFragment

class DecryptionFragmentTestRobot(fragmentArguments: Bundle) {
    private val screen = DecryptionFragmentScreen()

    private val navController: TestNavHostController = TestNavHostController(ApplicationProvider.getApplicationContext()).apply {
        setGraph(R.navigation.main_navigation)
        setCurrentDestination(R.id.decryption_fragment, fragmentArguments)
    }

    init {

        NavGraphHelper.initTestNavGraph<DecryptionFragment>(
                navController = navController,
                fragmentArguments = fragmentArguments
        )
    }

    fun selectAlgorithm(algorithmName: String) {
        screen {
            cipherAlgorithms {
                childWith<AlgorithmListItem> {
                    withDescendant { withText(algorithmName) }
                } perform {
                    click()
                }
            }
        }
    }

    fun enterDecryptionKey(key: String) {
        screen {
            keyDialogTextView {
                typeText(key)
            }
        }
    }

    fun verifyKeyDialog(isShown: Boolean) {
        screen {
            keyDialogTextView {
                if(isShown) {
                    isDisplayed()
                } else {
                    doesNotExist()
                }
            }
        }
    }

    fun verifyKeyDialogButton(isEnabled: Boolean) {
        screen {
            keyDialogButton {
                if (isEnabled) {
                    isEnabled()
                } else {
                    isDisabled()
                }
            }
        }
    }

    fun verifyNavigateToResult() {
        NavGraphHelper.assertNavGraphDestination(
                navController = navController,
                expectedDestinationId = R.id.result_fragment
        )

        //TODO: verify that correct data was passed to result fragment
    }

}