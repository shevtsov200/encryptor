package shevtsov.daniil.encryptor.fragment.result

import com.agoda.kakao.edit.KEditText
import com.agoda.kakao.screen.Screen
import com.agoda.kakao.text.KButton
import shevtsov.daniil.encryptor.R

class ResultFragmentScreen : Screen<ResultFragmentScreen>() {
    val encryptionKeyTextView = KEditText { withId(R.id.result_key_text) }
    val resultTextView = KEditText { withId(R.id.result_text_view) }

    val copyKeyButton = KButton { withId(R.id.copy_key_button) }
    val copyResultButton = KButton { withId(R.id.copy_result_button) }
    val saveTextButton = KButton { withId(R.id.save_button) }

    val fileDialogTextView = KEditText { withId(R.id.choose_file_name_edit_text) }
    val fileDialogButton = KEditText { withId(R.id.choose_file_button) }

}