package shevtsov.daniil.encryptor.fragment.encryption

import com.agoda.kakao.recycler.KRecyclerView
import com.agoda.kakao.screen.Screen
import shevtsov.daniil.encryptor.R
import shevtsov.daniil.encryptor.common.AlgorithmListItem

class EncryptionFragmentScreen : Screen<EncryptionFragmentScreen>() {
    val cipherAlgorithms = KRecyclerView({ withId(R.id.cipher_list_view) }, itemTypeBuilder = { itemType(::AlgorithmListItem) })

}