package shevtsov.daniil.encryptor.fragment.decryption

import com.agoda.kakao.edit.KEditText
import com.agoda.kakao.recycler.KRecyclerView
import com.agoda.kakao.screen.Screen
import com.agoda.kakao.text.KButton
import shevtsov.daniil.encryptor.R
import shevtsov.daniil.encryptor.common.AlgorithmListItem

class DecryptionFragmentScreen : Screen<DecryptionFragmentScreen>() {
    val cipherAlgorithms = KRecyclerView({ withId(R.id.cipher_list_view) }, itemTypeBuilder = { itemType(::AlgorithmListItem) })

    val keyDialogTextView = KEditText { withId(R.id.dialog_key_edit_text) }
    val keyDialogButton = KButton { withId(R.id.dialog_decrypt_button) }

}