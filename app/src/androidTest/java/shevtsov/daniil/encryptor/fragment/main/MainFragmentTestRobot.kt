package shevtsov.daniil.encryptor.fragment.main

import androidx.navigation.NavController
import androidx.navigation.NavDirections
import org.mockito.Mockito
import shevtsov.daniil.encryptor.R
import shevtsov.daniil.encryptor.common.NavGraphHelper
import shevtsov.daniil.encryptor.common.ViewActions
import shevtsov.daniil.encryptor.ui.main.MainFragment
import shevtsov.daniil.encryptor.ui.main.MainFragmentDirections

class MainFragmentTestRobot {
    private val screen = MainFragmentScreen()

    private val navController: NavController = Mockito.mock(NavController::class.java)

    init {
        NavGraphHelper.initTestNavGraph<MainFragment>(navController = navController)
    }

    fun enterText(sourceText: String) {
        screen {
            sourceTextView {
                typeText(text = sourceText)
            }

            closeSoftKeyboard()
        }
    }

    fun enterTextInstantly(sourceText: String) {
        screen {
            sourceTextView {
                perform {
                    ViewActions.writeTextInstantly(sourceText)
                }
            }

            closeSoftKeyboard()
        }
    }

    fun clickEncrypt() {
        screen {
            encryptButton {
                click()
            }
        }
    }

    fun clickDecrypt() {
        screen {
            decryptButton {
                click()
            }
        }
    }

    fun verifyEncryptButton(isEnabled: Boolean) {
        screen {
            encryptButton {
                if (isEnabled) {
                    isEnabled()
                } else {
                    isDisabled()
                }
            }
        }
    }

    fun verifyDecryptButton(isEnabled: Boolean) {
        screen {
            decryptButton {
                if (isEnabled) {
                    isEnabled()
                } else {
                    isDisabled()
                }
            }
        }

    }

    fun verifyNavigateToEncryption(expectedText: String) {
        val expectedDirection = MainFragmentDirections.navigateToEncryption(expectedText)
        verifyNavGraphDirection(expectedDirection)

        NavGraphHelper.assertNavGraphDestination(
                navController = navController,
                expectedDestinationId = R.id.encryption_fragment
        )

    }

    fun verifyNavigateToDecryption(expectedText: String) {
        val expectedDirection = MainFragmentDirections.navigateToDecryption(expectedText)
        verifyNavGraphDirection(expectedDirection)

        NavGraphHelper.assertNavGraphDestination(
                navController = navController,
                expectedDestinationId = R.id.decryption_fragment
        )

    }

    private fun verifyNavGraphDirection(expectedDirection: NavDirections) {
        Mockito.verify(navController).navigate(expectedDirection)
    }

}