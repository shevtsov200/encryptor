package shevtsov.daniil.encryptor.fragment.encryption

import androidx.test.ext.junit.runners.AndroidJUnit4
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import shevtsov.daniil.encryptor.ui.main.MainFragmentDirections

@RunWith(AndroidJUnit4::class)
class EncryptionFragmentTest {

    companion object {
        private const val TEST_PLAIN_TEXT = "lol"
        private const val TEST_CIPHER_NAME = "DES"

        private val fragmentArguments = MainFragmentDirections.navigateToEncryption(sourceText = TEST_PLAIN_TEXT).arguments
    }

    @Before
    fun onSetup() {

    }

    @Test
    fun testNavigateToResult() = withDecryptionTestRobot{
        selectAlgorithm(algorithmName = TEST_CIPHER_NAME)

        verifyNavigateToResult()
    }

    private fun withDecryptionTestRobot(fn: EncryptionFragmentTestRobot.() -> Unit) = EncryptionFragmentTestRobot(fragmentArguments).run(fn)

}