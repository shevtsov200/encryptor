package shevtsov.daniil.encryptor.fragment.main

import androidx.preference.PreferenceManager
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import shevtsov.daniil.encryptor.util.validation.MaxTextLengthProvider
import java.util.Arrays

@RunWith(AndroidJUnit4::class)
class MainFragmentTest {

    companion object {
        //TODO: This test knows too much
        private const val MAX_SOURCE_TEXT_CHARACTER_COUNT = MaxTextLengthProvider.MAX_ALLOWED_TEXT_LENGTH
        private const val INVALID_CHARACTER_COUNT = MAX_SOURCE_TEXT_CHARACTER_COUNT + 1

        private const val TEST_ENCRYPTION_TEXT = "zzz zzz"
        private const val TEST_DECRYPTION_TEXT = "FFFFF"
    }

    @Before
    fun onSetup() {
        clearSharedPreferences()
    }

    @Test
    fun testNavigateToEncryption() = withMainFragmentRobot {
        enterText(sourceText = TEST_ENCRYPTION_TEXT)
        clickEncrypt()

        verifyNavigateToEncryption(expectedText = TEST_ENCRYPTION_TEXT)
    }

    @Test
    fun testNavigateToDecryption() = withMainFragmentRobot {
        enterText(sourceText = TEST_DECRYPTION_TEXT)
        clickDecrypt()

        verifyNavigateToDecryption(TEST_DECRYPTION_TEXT)
    }

    @Test
    fun testChooseButtonsDisabledAtStart() = withMainFragmentRobot {
        verifyDecryptButton(isEnabled = false)
        verifyEncryptButton(isEnabled = false)
    }

    @Test
    fun testEncryptionButtonEnabledWhenTextValid() = withMainFragmentRobot {
        enterText(sourceText = TEST_ENCRYPTION_TEXT)

        verifyEncryptButton(isEnabled = true)
    }

    @Test
    fun testDecryptionButtonEnabledWhenTextValid() = withMainFragmentRobot {
        enterText(sourceText = TEST_DECRYPTION_TEXT)

        verifyDecryptButton(isEnabled = true)
    }

    @Test
    fun testTextLargerThanMaxWordCount() = withMainFragmentRobot {
        enterTextInstantly(sourceText = generateLargeText())

        verifyEncryptButton(isEnabled = false)
        verifyDecryptButton(isEnabled = false)
    }

    private fun withMainFragmentRobot(fn: MainFragmentTestRobot.() -> Unit) = MainFragmentTestRobot().run(fn)

    private fun generateLargeText(): String {
        val repeatedCharacter = CharArray(INVALID_CHARACTER_COUNT)
        Arrays.fill(repeatedCharacter, 'c')
        return String(repeatedCharacter)
    }

    //TODO: replace this workaround with something that does not clear real shared preferences
    private fun clearSharedPreferences() {
        val preferences = PreferenceManager.getDefaultSharedPreferences(ApplicationProvider.getApplicationContext())
        preferences.edit().clear().commit()
    }

}